//(c) Michael O'Neil 2015

#ifndef BOSS_H
#define BOSS_H

/*
* Boss
*/
class Boss : public NPC {
public:
	~Boss() {}
	Boss(std::string mID);
	float getXPos(void) {return xPos;}
	float getYPos(void) {return yPos;}
	float getXSize(void) {return xSize;}
	float getYSize(void) {return ySize;}
	std::string getID(void) {return ID;}
	std::string getName(void) {return name;}
	int getHealth(void) {return health;}
	int getStrength(void) {return strength;}
	int getSpeed(void) {return speed;}
	int getDollars(void) {return dollars;}
	int getItemChance(void) {return itemChance;}
	GLuint getTex(void) {return texture;}	
	void setHealth(int);
	void setDollars(int);
	void draw(void);
	void newPos(void);	
	void updateAttributes(int, int, int, int);
	void setTex(GLuint tex) {texture = tex;}
};

#endif