//(c) Michael O'Neil 2015

#ifndef GAMEOVERSTATE_H
#define GAMEOVERSTATE_H

/*
* Game Over State - Displays text if player wins or loses the game.
*/
class GameOverState : public GameState {
private:
	bool monsterAlive;	
public:			
	void draw(SDL_Window * window, Game &context);	
	void init(Game &context);	
	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);	
	void enter(Game &context);
	void exit();	
};

#endif