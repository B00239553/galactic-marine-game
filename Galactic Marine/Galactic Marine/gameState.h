//(c) Michael O'Neil 2015

#include <SDL.h>
#include "label.h"

class Game;

/*
* Abstract Game State Class.
* Different game states inherit from this.
*/
class GameState {
public:	
	virtual ~GameState() { return; } // virtual destructor
	virtual void draw(SDL_Window * window, Game &context) = 0;
	virtual void init(Game &context) = 0;
	virtual void handleSDLEvent(SDL_Event const &sdlEvent, Game &context) = 0;	
	virtual void enter(Game &context) = 0;
	virtual void exit() = 0;	
protected:
	Label* screen;	
};