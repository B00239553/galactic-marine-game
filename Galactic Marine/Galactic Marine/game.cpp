//(c) Michael O'Neil 2015

#include "game.h";
#include "Utilities.h"
#include "mainMenuState.h"
#include "introState.h"
#include "creditsState.h"
#include "characterState.h"
#include "mapState.h"
#include "combatState.h"
#include "gameOverState.h"
#include "tableState.h"
#include "instructionState.h"
#include "Player.h"
#include "Raider.h"
#include "Brute.h"
#include "Fodder.h"
#include "Boss.h"
#include "CombatPack.h"
#include "HealthPack.h"
#include "Stimulant.h"

/*
* Constructor - Sets count & itemCount to zero, these are used for the monsterArray & itemArray.
*/
Game::Game()
{
	count = 0;	
	itemCount = 0;		
	updateScore = false;	
}

/*
* Initialises all the states and creates the player.
*/
void Game::init()
{	
	SDL_GLContext glContext; // OpenGL context handle
	SDL_Window *window = setupRC(glContext); // Create window and render context 	
	State_intro = new IntroState();
	State_intro->init(*this);
	State_mainMenu = new MainMenuState();
	State_mainMenu->init(*this);
	State_credits = new CreditsState();
	State_credits->init(*this);
	State_character = new CharacterState();
	State_character->init(*this);
	State_map = new MapState();
	State_map-> init(*this);
	State_combat = new CombatState();
	State_combat->init(*this);
	State_gameOver = new GameOverState();
	State_gameOver->init(*this);
	State_HighScore = new TableState();
	State_HighScore->init(*this);
	State_Instructions = new InstructionState();
	State_Instructions->init(*this);
	currentState = State_intro;		
	my_Player = NULL;
	running = true;		
	
	char1 = loadTexture("Character1.bmp", 216, 285);	
	char2 = loadTexture("Character2.bmp", 216, 256);
	char3 = loadTexture("Character3.bmp", 180, 255);
	fodder = loadTexture("Fodder.bmp", 460, 415);
	brute = loadTexture("Brute.bmp", 460, 415);
	raider = loadTexture("Raider.bmp", 540, 498);
	boss = loadTexture("The Boss.bmp", 540, 498);
	health = loadTexture("healthpack.bmp", 216, 216);
	strength = loadTexture("strength.bmp", 216, 236);
	stimulant = loadTexture("stimulant.bmp", 216, 226);
}

/*
* Set up rendering context.
* Sets values for, and creates an OpenGL context for use with SDL.
* @param - SDL_GLContext - SDL_GL context.
* @return - SDL_Window - returns the SDL window.
*/
SDL_Window * Game::setupRC(SDL_GLContext &context)
{
	
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        Utility::exitFatalError("Unable to initialize SDL"); 

    // Request an OpenGL 2.1 context.
	// If you request a context not supported by your drivers, no OpenGL context will be created
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering

	// Optional: Turn on x4 multisampling anti-aliasing (MSAA)
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
 
    // Create 800x600 window
    window = SDL_CreateWindow("Galactic Marine",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        Utility::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate

	// set up TrueType / SDL_ttf
	if (TTF_Init()== -1)
		Utility::exitFatalError("TTF failed to initialise.");

	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	if (textFont == NULL)
		Utility::exitFatalError("Failed to open font.");

	return window;
} 

/*
* Loads in bitmap file and creates a texture.
* @param - const char* - the filename.
* @param - int - width of file.
* @param - int - height of file.
* @return - GLuint - the texture to be returned.
*/
GLuint Game::loadTexture(const char * filename, int width, int height)
{
	GLuint texture;
	unsigned char * data;
	FILE * file;

	file = fopen(filename, "rb");
	if (file == NULL) return 0;

	data = (unsigned char *)malloc(width * height * 3);

	fread(data, width * height * 3, 1, file);
	fclose(file);
		
	for(int i = 0; i < width * height ; ++i)
	{
		int index = i*3;
		unsigned char B,R;
		B = data[index];
		R = data[index+2];

		data[index] = R;
		data[index+2] = B;
	}

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,width,height,0,GL_RGB,GL_UNSIGNED_BYTE,data);
	
    free( data );
	return texture;
}

void Game::deleteTex(GLuint tex)
{
	glDeleteTextures( 1, &tex );
}

/*
* Used for changing states, it will perform any exit requirements
* for the current state. It will then enter the new state and perform any
* entry requirements.
* @param - GameState - The new game state to be entered.
*/
void Game::setState(GameState* newState)
{
	if(newState != currentState)  
	{		
		currentState->exit();
		currentState = newState;	
		currentState->enter(*this);		
	}
}

/*
* Creates player character;
*/
void Game::createPlayer()
{
	my_Player = new Player();	
}
 
/*
* Deletes player character;
*/
void Game::deletePlayer()
{
	if(my_Player != NULL)
	{
		delete my_Player;
		my_Player = NULL;
	}
}

/*
* Creates any NPC unit, it will create the certain type depending
* on what parameter has been input, it will only create the unit if 
* there is space in the monsterArray.
* @param - std::string - the type of NPC to be created.
* @param - std::string - the ID of the NPC to be created.
*/
void Game::createNPC(std::string type, std::string ID, GLuint tex)
{
	if(count < MAX)
	{
		if(type == "Brute")
		{
			newNPC = new Brute(ID);
			monsterArray[count] = newNPC;
			count++;
			newNPC->setTex(brute);
		}
		else if(type == "Fodder")
		{
			newNPC = new Fodder(ID);
			monsterArray[count] = newNPC;
			count++;
			newNPC->setTex(fodder);
		}
		else if(type == "Raider")
		{
			newNPC = new Raider(ID);
			monsterArray[count] = newNPC;
			count++;
			newNPC->setTex(raider);
		}
		else
		{
			newNPC = new Boss(ID);
			monsterArray[count] = newNPC;
			count++;
			newNPC->setTex(boss);
		}
	}	
}

/*
* Gets the NPC in the array at the position matched.
* @param - int - the position in the monster array to be obtained.
* @return - NPC - returns the NPC from the array.
*/
NPC *Game::getNPC(int i)
{	
	currentNPC = monsterArray[i];
	return currentNPC;
}

/*
* Deletes the NPC taken in the parameter, it will also move
* elements in the array and reduce the current count. 
* @param - NPC* - the NPC to be deleted.
*/
void Game::deleteNPC(NPC* thisNPC)
{	
	int index = 0;	
	while(thisNPC != monsterArray[index])
	{
		monsterArray[index++];		
	}	
	delete thisNPC;	
	thisNPC = NULL;

	for(;index < (count - 1); index++)
	{
		monsterArray[index] = monsterArray[index+1];
	}
	count--;	
}

/*
* Deletes all monsters still held.
*/
void Game::deleteAllNPC()
{
	if(count > 0)
	{
		for(int index = 0; index < count; index++)
		{
			currentNPC = monsterArray[index];
			delete currentNPC;	
			currentNPC = NULL;
		}	
		count = 0;	
	}
}

/*
* Creates any Item, it will create the certain type depending
* on what parameter has been input, it will only create the unit if 
* there is space in the itemArray.
* @param - std::string - the type of item to be created.
* @param - std::string - the ID of the item to be created.
*/
void Game::createItem(std::string type, std::string ID)
{
	if(itemCount < MAXITEM)
	{
		if(type == "HealthPack")
		{
			newItem = new HealthPack(ID);
			itemArray[itemCount] = newItem;
			itemCount++;
			newItem->setTex(health);
		}
		else if(type == "CombatPack")
		{
			newItem = new CombatPack(ID);
			itemArray[itemCount] = newItem;
			itemCount++;
			newItem->setTex(strength);
		}
		else
		{
			newItem = new Stimulant(ID);
			itemArray[itemCount] = newItem;
			itemCount++;
			newItem->setTex(stimulant);
		}
	}	
}

/*
* Gets the Item in the array at the position matched.
* @param - int - the position in the item array to be obtained.
* @return - Item - returns the item from the array.
*/
Item *Game::getItems(int i)
{	
	currentItem = itemArray[i];
	return currentItem;
}
 
/*
* Deletes the item taken in the parameter, it will also move
* elements in the array and reduce the current itemCount.
* @param - Items* - the item to be deleted.
*/
void Game::deleteItem(Item* thisItem)
{	
	int index = 0;	
	while(thisItem != itemArray[index])
	{
		itemArray[index++];		
	}	
	delete thisItem;	
	thisItem = NULL;

	for(;index < (itemCount - 1); index++)
	{
		itemArray[index] = itemArray[index+1];
	}
	itemCount--;	
}

/*
* Deletes all items still held.
*/
void Game::deleteAllItems()
{
	if(itemCount > 0)
	{
		for(int index = 0; index < itemCount; index++)
		{
			currentItem = itemArray[index];
			delete currentItem;		
			currentItem = NULL;
		}
		itemCount = 0;	
	}
}

/*
* Checks if monster or item created is colliding with player, another
* monster or item. If it is will set new random position for object.
* @param - NPC* - the NPC to be checked.
* @param - Item - the Item to be checked.
* @return = bool - returns true if collision detected and has set new position, false if no collisions.
*/
bool Game::collisionAtCreation(NPC* thisNPC, Item* thisItem)
{
	bool collision = false;
	if(my_Player != NULL && thisNPC != NULL)
	{
		collision = checkCollision(my_Player->getXPos(), my_Player->getXSize(), my_Player->getYPos(), my_Player->getYSize(),
			thisNPC->getXPos(), thisNPC->getXSize(), thisNPC->getYPos(), thisNPC->getYSize());
		if(collision)
		{
			thisNPC->newPos();
			return true;
		}
	}

	if(my_Player != NULL && thisItem != NULL)
	{
		collision = checkCollision(thisItem->getXPos(), thisItem->getXSize(), thisItem->getYPos(), thisItem->getYSize(),
			my_Player->getXPos(), my_Player->getXSize(), my_Player->getYPos(), my_Player->getYSize());
		if(collision)
		{
			thisItem->newPos();
			return true;
		}
	}

	if(count != 0 && thisNPC != NULL)
	{
		int i = 0, j = 0;
		while(collision == false && i < count)
		{
			if(thisNPC->getID() != monsterArray[i]->getID())
				collision = checkCollision(thisNPC->getXPos(), thisNPC->getXSize(), thisNPC->getYPos(), thisNPC->getYSize(),
			monsterArray[i]->getXPos(), monsterArray[i]->getXSize(), monsterArray[i]->getYPos(), monsterArray[i]->getYSize());			
			i++;
		}
		
		if(itemCount != 0)
			while(collision == false && j < itemCount)
			{
				collision = checkCollision(thisNPC->getXPos(), thisNPC->getXSize(), thisNPC->getYPos(), thisNPC->getYSize(),
					itemArray[j]->getXPos(), itemArray[j]->getXSize(), itemArray[j]->getYPos(), itemArray[j]->getYSize());				
				j++;
			}

		if(collision)
		{
			thisNPC->newPos();
			return true;
		}
	}

	if(itemCount != 0 && thisItem != NULL)
	{
		int i = 0, j = 0;
		while(collision == false && i < itemCount)
		{
			if(thisItem->getID() != itemArray[i]->getID())
				collision = checkCollision(thisItem->getXPos(), thisItem->getXSize(), thisItem->getYPos(), thisItem->getYSize(),
			itemArray[i]->getXPos(), itemArray[i]->getXSize(), itemArray[i]->getYPos(), itemArray[i]->getYSize());
			i++;
		}
		
		if(count != 0)
			while(collision == false && j < count)
			{			
				collision = checkCollision(thisItem->getXPos(), thisItem->getXSize(), thisItem->getYPos(), thisItem->getYSize(),
					monsterArray[j]->getXPos(), monsterArray[j]->getXSize(), monsterArray[j]->getYPos(), monsterArray[j]->getYSize());
				j++;
			}

		if(collision)
		{
			thisItem->newPos();
			return true;
		}
	}
	return false;
}
	
/*
* Checks if two object values taken in have collided.
* @param - float - object A x coordinate.
* @param - float - object A x size.
* @param - float - object A y coordinate.
* @param - float - object A y size.
* @param - float - object B x coordinate.
* @param - float - object B x size.
* @param - float - object B y coordinate.
* @param - float - object B y size.
* @return - bool - returns true if collision detected, false otherwise.
*/
bool Game::checkCollision(float objAX, float objAXSize, float objAY, float objAYSize, float objBX, float objBXSize, float objBY, float objBYSize)
{
	if(objAX <= objBX && (objAX + objAXSize) >= objBX && (objAY + objAYSize) >= objBY && objAY < objBY)
		return true;
	if(objAX <= objBX && (objAX + objAXSize) >= objBX && objAY <= (objBY + objBYSize) && (objAY + objAYSize) >= (objBY + objBYSize))
		return true;
	if(objAX <= (objBX + objBXSize) && (objAX + objAXSize) >= (objBX + objBXSize) && (objAY + objAYSize) >= objBY && objAY < objBY)
		return true;
	if(objAX <= (objBX + objBXSize) && (objAX + objAXSize) >= (objBX + objBXSize) && objAY <= (objBY + objBYSize) && (objAY + objAYSize) >= (objBY + objBYSize))
		return true;	
	if(objAX >= objBX && (objAX + objAXSize) <= (objBX + objBXSize) && objAY <= (objBY + objBYSize) && (objAY + objAYSize) >= (objBY + objBYSize))
		return true;
	if(objAX >= objBX && (objAX + objAXSize) <= (objBX + objBXSize) && (objAY + objAYSize) >= objBY && objAY <= objBY)
		return true;
	if(objAX <= (objBX + objBXSize) && (objAX + objAXSize) >= (objBX + objBXSize) && objAY >= objBY && (objAY + objAYSize) <= (objBY + objBYSize))
		return true;
	if(objAX <= objBX && (objAX + objAXSize) >= objBX && objAY >= objBY && (objAY + objAYSize) <= (objBY + objBYSize))
		return true;
	if(objAX >= objBX && (objAX + objAXSize) <= (objBX + objBXSize) && objAY >= objBY && (objAY + objAYSize) <= (objBY + objBYSize))
		return true;
	return false;
}

/*
* Main Game Loop.
*/
void Game::run(void) 
{			
	SDL_Event sdlEvent; // variable to detect SDL events	

	while (running)	// the event loop
	{
		while (SDL_PollEvent(&sdlEvent))
		{
			if (sdlEvent.type == SDL_QUIT)
				running = false;
			else
				currentState->handleSDLEvent(sdlEvent, *this);

		}		
		currentState->draw(window, *this); // call the draw function		
	}	
}