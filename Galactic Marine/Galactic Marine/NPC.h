//(c) Michael O'Neil 2015

class Game;

/*
* Abstract NPC Class, other NPCs will inherit from this class.
*/
class NPC {
protected:		
	std::string ID;
	std::string name;
	float xPos;
	float yPos;
	float xSize;
	float ySize;
	int health;
	int strength;
	int speed;
	int dollars;
	int itemChance;	
	GLuint texture;
public:		
	virtual ~NPC() {}
	virtual float getXPos() = 0;
	virtual float getYPos() = 0;
	virtual float getXSize() = 0;
	virtual float getYSize() = 0;
	virtual std::string getID() = 0;
	virtual std::string getName() = 0;
	virtual int getHealth() = 0;
	virtual int getStrength() = 0;
	virtual int getSpeed() = 0;
	virtual int getDollars() = 0;
	virtual int getItemChance() = 0;
	virtual GLuint getTex() = 0;	
	virtual void setHealth(int) = 0;
	virtual void setDollars(int) = 0;
	virtual void draw() = 0;
	virtual void newPos() = 0;
	virtual void setTex(GLuint tex) = 0;
	virtual void updateAttributes(int, int, int, int) = 0;
};