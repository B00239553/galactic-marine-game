//(c) Michael O'Neil 2015

#ifndef STIMULANT_H
#define STIMULANT_H

/*
* Stimulant Item
*/
class Stimulant : public Item {
public:		
	~Stimulant() {}
	Stimulant(std::string mID);
	float getXPos() {return xPos;}
	float getYPos() {return yPos;}
	float getXSize() {return xSize;}
	float getYSize() {return ySize;}
	GLuint getTex() {return texture;}
	std::string getID() {return ID;}
	std::string getName() {return name;}
	int getEffect() {return effect;}	
	void draw();
	void newPos(void);
	void setTex(GLuint tex) {texture = tex;}
};

#endif