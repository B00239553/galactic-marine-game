//(c) Michael O'Neil 2015

#include<cstdlib>
#include "game.h"
#include "Boss.h"

/*
* Constructor
* @param - std::string - ID of monster.
*/
Boss::Boss(std::string mID)
{			
	ID = mID;
	name = "Boss";
	xPos = (float)rand()/RAND_MAX - (float)rand()/RAND_MAX;
	yPos = (float)rand()/RAND_MAX - (float)rand()/RAND_MAX;
	if(yPos > 0.85)
		yPos = 0.85;
	if(yPos < -0.99)
		yPos = -0.99;
	if(xPos > 0.85)
		xPos = 0.85;
	if(xPos < -0.99)
		xPos = -0.99;
	xSize = 0.2f;
	ySize = 0.2f;
	health = 25;
	strength = 20;
	speed = 12;
	dollars = 1000;
	itemChance = 0;	
}

/*
* Sets health for during/after combats.
* @param - int - health to be set.
*/
void Boss::setHealth(int mHealth)
{
	health = mHealth;	
}

/*
* Sets dollars for after combats.
* @param - int - dollars to be set.
*/
void Boss::setDollars(int Dollars)
{
	dollars = Dollars;	
}

/*
* Draws the NPC so can be called in required functions.
*/
void Boss::draw()
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture( GL_TEXTURE_2D, texture);	
	glBegin(GL_POLYGON);	
	glTexCoord2d(1.0,1.0);	
	glVertex3f (xPos, yPos+ySize, 0.0); // first corner
	glTexCoord2d(0.0,1.0); 	
	glVertex3f (xPos+xSize, yPos+ySize, 0.0); // second corner
	glTexCoord2d(0.0,0.0); 
	glVertex3f (xPos+xSize, yPos, 0.0); // third corner
	glTexCoord2d(1.0,0.0); 
	glVertex3f (xPos, yPos, 0.0); // fourth corner
	glEnd();		
	glDisable(GL_TEXTURE_2D);	
}

/*
* Used to set new Position as collides with another object already held.
*/
void Boss::newPos()
{
	xPos = (float)rand()/RAND_MAX - (float)rand()/RAND_MAX;
	yPos = (float)rand()/RAND_MAX - (float)rand()/RAND_MAX;
	if(yPos > 0.85)
		yPos = 0.85;
	if(yPos < -0.99)
		yPos = -0.99;
	if(xPos > 0.85)
		xPos = 0.85;
	if(xPos < -0.99)
		xPos = -0.99;
}

/*
* Updates attributes for different levels, no action for boss as only one.
*/
void Boss::updateAttributes(int mHealth, int mSpeed, int mStrength, int mDollars)
{
	
}