//(c) Michael O'Neil 2015

#ifndef INTROSTATE_H   
#define INTROSTATE_H   

/*
* Intro State - Used at game load only, player can bypass by pressing
* any key, will automatically change after 30 seconds.
*/
class IntroState : public GameState {
private:
	bool story;	
public:			
	void draw(SDL_Window * window, Game &context);	
	void init(Game &context);	
	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);	
	void enter(Game &context);
	void exit();		
};

#endif 