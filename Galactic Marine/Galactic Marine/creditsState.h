//(c) Michael O'Neil 2015

#ifndef CREDITSSTATE_H
#define CREDITSSTATE_H
#include <ctime>

/*
* Credits - Displays Credits, will be shown for 30 seconds unless
* any key is pressed, will return to main menu.
*/
class CreditsState : public GameState {
public:			
	void draw(SDL_Window * window, Game &context);	
	void init(Game &context);	
	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);	
	void enter(Game &context);
	void exit();	
private:
	//used to store clock times
	clock_t start;
	clock_t finish;	
};

#endif