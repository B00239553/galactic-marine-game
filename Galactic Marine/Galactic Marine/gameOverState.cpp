//(c) Michael O'Neil 2015

#include "game.h"
#include "gameOverState.h"
#include <ctime>
#include <time.h>
#include <sstream>

/*
* @param - Game - the context of the game.
*/
void GameOverState::init(Game &context)
{	
	screen = new Label();
}

/*
* Checks to see if monsters alive, player loses if any
* monsters survive, wins otherwise.
* @param - Game - the context of the game.
*/
void GameOverState::enter(Game &context)
{	
	if(context.getCount() > 0)
		monsterAlive = true;
	else
		monsterAlive = false;	
	context.setUpdateScore(true);	
}

/*
* Displays text to state if player has won or lost.
* @param - SDL_window - used for drawing to screen.
* @param - Game - the context of the game.
*/
void GameOverState::draw(SDL_Window * window, Game &context)
{			
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window		
	screen->textToTexture("Game Over", 80);
	screen->draw(-0.4f, 0.2f);
	if(monsterAlive) //displayed only if player loses.			
	{
		screen->textToTexture("You Lost", 40);
		screen->draw(-0.14f, 0.0f);
	}	
	else //displayed only if player wins.		
	{
		screen->textToTexture("You Win", 40);
		screen->draw(-0.14f, 0.0f);
	}	
	std::stringstream strStream;		
	strStream << "Final Score �" << context.getPlayer()->getDollars();	
	screen->textToTexture(strStream.str().c_str(), 25);
	screen->draw(-0.15f, -0.15f); 

	SDL_GL_SwapWindow(window);		
}

/*
* Handles any events that can be used in this state.
* @param - SDL_Event - used for keyboard events.
* @param - Game - the context of the game.
*/
void GameOverState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{		
	if (sdlEvent.type == SDL_KEYDOWN)
	{		
		context.setState(context.getTableState());		
	}	
}

/*
* No exit requirements.
*/
void GameOverState::exit()
{
}