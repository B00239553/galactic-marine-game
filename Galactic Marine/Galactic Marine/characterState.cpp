//(c) Michael O'Neil 2015

#include "game.h"
#include "characterState.h"

/*
* @param - Game - the context of the game.
*/
void CharacterState::init(Game &context)
{
	screen = new Label();	
}

/*
* Creates player, monsters and items for the new game.
* @param - Game - the context of the game.
*/
void CharacterState::enter(Game &context)
{		
	playerName = "_____________";
	context.createPlayer();	
	context.createNPC("Raider", "Raider1", context.getTexRaider()); //creates new NPC
	while(context.collisionAtCreation(context.getNPC(0), NULL));
	context.createNPC("Brute", "Brute1", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(1), NULL));
	context.createNPC("Brute", "Brute2", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(2), NULL));
	context.createNPC("Brute", "Brute3", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(3), NULL));
	context.createNPC("Fodder", "Fodder1", context.getTexFodder());
	while(context.collisionAtCreation(context.getNPC(4), NULL));
	context.createNPC("Fodder", "Fodder2", context.getTexFodder());
	while(context.collisionAtCreation(context.getNPC(5), NULL));
	context.createNPC("Fodder", "Fodder3", context.getTexFodder());
	while(context.collisionAtCreation(context.getNPC(6), NULL));
	context.createNPC("Fodder", "Fodder4", context.getTexFodder());
	while(context.collisionAtCreation(context.getNPC(7), NULL));
	context.createNPC("Fodder", "Fodder5", context.getTexFodder());
	while(context.collisionAtCreation(context.getNPC(8), NULL));
	context.createItem("HealthPack", "HealthPack1"); //creates new Items
	while(context.collisionAtCreation(NULL, context.getItems(0)));
	context.createItem("HealthPack", "HealthPack2");
	while(context.collisionAtCreation(NULL, context.getItems(1)));
	context.createItem("CombatPack", "CombatPack1");
	while(context.collisionAtCreation(NULL, context.getItems(2)));
	context.createItem("CombatPack", "CombatPack2");
	while(context.collisionAtCreation(NULL, context.getItems(3)));
	context.createItem("Stimulant", "Stimulant1");
	while(context.collisionAtCreation(NULL, context.getItems(4)));
	character1 = false;
	character2 = false;
	character3 = false;
}

/*
* Allows player to enter their name.
* Displays text to state what character to choose and their attributes.
* @param - SDL_window - used for drawing to screen.
* @param - Game - the context of the game.
*/
void CharacterState::draw(SDL_Window * window, Game &context)
{	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	screen->textToTexture("Character Selection", 40);
	screen->draw(-0.4, 0.8);	
	screen->textToTexture("Please enter name (A-Z):", 25);
	screen->draw(-0.35, 0.6);	
	screen->textToTexture(playerName.c_str(), 25);
	screen->draw(-0.25, 0.5);
	screen->textToTexture("Select Character and then press return", 25);
	screen->draw(-0.5, 0.2);	
	screen->textToTexture("Press 1", 25);
	screen->draw(-0.46, 0.0);
	screen->textToTexture("Health: 11", 25);
	screen->draw(-0.5, -0.4);
	screen->textToTexture("Speed: 10", 25);
	screen->draw(-0.5, -0.5);
	screen->textToTexture("Strength: 10", 25);
	screen->draw(-0.5, -0.6);	
	screen->textToTexture("Press 2", 25);
	screen->draw(-0.11, 0.0);
	screen->textToTexture("Health: 10", 25);
	screen->draw(-0.15, -0.4);
	screen->textToTexture("Speed: 10", 25);
	screen->draw(-0.15, -0.5);
	screen->textToTexture("Strength: 11", 25);
	screen->draw(-0.15, -0.6);	
	screen->textToTexture("Press 3", 25);
	screen->draw(0.24, 0.0);
	screen->textToTexture("Health: 10", 25);
	screen->draw(0.2, -0.4);
	screen->textToTexture("Speed: 11", 25);
	screen->draw(0.2, -0.5);
	screen->textToTexture("Strength: 10", 25);
	screen->draw(0.2, -0.6);	
	
    glEnable(GL_TEXTURE_2D);
	glBindTexture( GL_TEXTURE_2D, context.getTexChar1());	
	glBegin(GL_POLYGON);	
	glTexCoord2d(0.0,0.0);	
    glVertex3f (-0.5, -0.3, 0.0); // first corner
	glTexCoord2d(1.0,0.0); 	
    glVertex3f (-0.3, -0.3, 0.0); // second corner
	glTexCoord2d(1.0,1.0); 
    glVertex3f (-0.3, -0.1, 0.0); // third corner
	glTexCoord2d(0.0,1.0); 
    glVertex3f (-0.5, -0.1, 0.0); // fourth corner
    glEnd();	
		
	glBindTexture( GL_TEXTURE_2D, context.getTexChar2());
	glBegin(GL_POLYGON);
	glTexCoord2d(0.0,0.0);
    glVertex3f (-0.15, -0.3, 0.0); // first corner
	glTexCoord2d(1.0,0.0); 	
    glVertex3f (0.05, -0.3, 0.0); // second corner
	glTexCoord2d(1.0,1.0); 
    glVertex3f (0.05, -0.1, 0.0); // third corner
	glTexCoord2d(0.0,1.0); 
    glVertex3f (-0.15, -0.1, 0.0); // fourth corner
    glEnd();	
		
	glBindTexture( GL_TEXTURE_2D, context.getTexChar3());
	glBegin(GL_POLYGON);
	glTexCoord2d(0.0,0.0);
    glVertex3f (0.2, -0.3, 0.0); // first corner
	glTexCoord2d(1.0,0.0); 	
    glVertex3f (0.4, -0.3, 0.0); // second corner
	glTexCoord2d(1.0,1.0); 
    glVertex3f (0.4, -0.1, 0.0); // third corner
	glTexCoord2d(0.0,1.0); 
    glVertex3f (0.2, -0.1, 0.0); // fourth corner
    glEnd();	

	glDisable(GL_TEXTURE_2D);

	if(character1 == true)
	{
		screen->textToTexture("Character 1 selected", 25);
		screen->draw(-0.3, -0.8);		
	}
	else if(character2 == true)
	{
		screen->textToTexture("Character 2 selected", 25);
		screen->draw(-0.3, -0.8);		
	}
	else if(character3 == true)
	{
		screen->textToTexture("Character 3 selected", 25);
		screen->draw(-0.3, -0.8);		
	}

	SDL_GL_SwapWindow(window);			
}

/*
* Handles any events that can be used in this state.
* @param - SDL_Event - used for keyboard events.
* @param - Game - the context of the game.
*/
void CharacterState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{		
		switch( sdlEvent.key.keysym.sym )
		{	
			case SDLK_BACKSPACE:
				if(playerName.size() > 0)			
					playerName.resize(playerName.size()-1);	
				if(playerName.size() == 0)				
					playerName = "_____________";
				break;
			case SDLK_RETURN:
				if(character1 == true || character2 == true || character3 == true)
				{
					context.getPlayer()->setDollars(0);
					if(playerName == "_____________")
					{
						playerName = "Player";
						context.getPlayer()->setName(playerName);
					}
					else
						context.getPlayer()->setName(playerName);
					context.setState(context.getMapState());	
				}
				break;	
			case SDLK_1:
				context.getPlayer()->setPos(-0.9f, 0.15f, 0.7f, 0.15f);
				context.getPlayer()->setHealth(11);
				context.getPlayer()->setHealthMax(11);
				context.getPlayer()->setStrength(10);
				context.getPlayer()->setSpeed(10);	
				context.getPlayer()->setTex(context.getTexChar1());				
				character1 = true;
				character2 = false;
				character3 = false;
				break;
			case SDLK_2:
				context.getPlayer()->setPos(-0.9f, 0.15f, 0.7f, 0.15f);
				context.getPlayer()->setHealth(10);
				context.getPlayer()->setHealthMax(10);
				context.getPlayer()->setStrength(11);
				context.getPlayer()->setSpeed(10);		
				context.getPlayer()->setTex(context.getTexChar2());		
				character1 = false;
				character2 = true;
				character3 = false;
				break;
			case SDLK_3:
				context.getPlayer()->setPos(-0.9f, 0.15f, 0.7f, 0.15f);
				context.getPlayer()->setHealth(10);
				context.getPlayer()->setHealthMax(10);
				context.getPlayer()->setStrength(10);
				context.getPlayer()->setSpeed(11);		
				context.getPlayer()->setTex(context.getTexChar3());	
				character1 = false;
				character2 = false;
				character3 = true;
				break;
			case 'a': case 'A':
				if(playerName == "_____________")
					playerName = "A";
				else
					playerName += "a";
				break;
			case 'b': case 'B':
				if(playerName == "_____________")
					playerName = "B";
				else
					playerName += "b";
				break;
			case 'c': case 'C':
				if(playerName == "_____________")
					playerName = "C";
				else
					playerName += "c";
				break;
			case 'd': case 'D':
				if(playerName == "_____________")
					playerName = "D";
				else
					playerName += "d";
				break;
			case 'e': case 'E':
				if(playerName == "_____________")
					playerName = "E";
				else
					playerName += "e";
				break;
			case 'f': case 'F':
				if(playerName == "_____________")
					playerName = "F";
				else
					playerName += "f";
				break;
			case 'g': case 'G':
				if(playerName == "_____________")
					playerName = "G";
				else
					playerName += "g";
				break;
			case 'h': case 'H':
				if(playerName == "_____________")
					playerName = "H";
				else
					playerName += "h";
				break;
			case 'i': case 'I':
				if(playerName == "_____________")
					playerName = "I";
				else
					playerName += "i";
				break;
			case 'j': case 'J':
				if(playerName == "_____________")
					playerName = "J";
				else
					playerName += "j";
				break;
			case 'k': case 'K':
				if(playerName == "_____________")
					playerName = "K";
				else
					playerName += "k";
				break;
			case 'l': case 'L':
				if(playerName == "_____________")
					playerName = "L";
				else
					playerName += "l";
				break;
			case 'm': case 'M':
				if(playerName == "_____________")
					playerName = "M";
				else
					playerName += "m";
				break;
			case 'n': case 'N':
				if(playerName == "_____________")
					playerName = "N";
				else
					playerName += "n";
				break;
			case 'o': case 'O':
				if(playerName == "_____________")
					playerName = "O";
				else
					playerName += "o";
				break;
			case 'p': case 'P':
				if(playerName == "_____________")
					playerName = "P";
				else
					playerName += "p";
				break;
			case 'q': case 'Q':
				if(playerName == "_____________")
					playerName = "Q";
				else
					playerName += "q";
				break;
			case 'r': case 'R':
				if(playerName == "_____________")
					playerName = "R";
				else
					playerName += "r";
				break;
			case 's': case 'S':
				if(playerName == "_____________")
					playerName = "S";
				else
					playerName += "s";
				break;
			case 't': case 'T':
				if(playerName == "_____________")
					playerName = "T";
				else
					playerName += "t";
				break;
			case 'u': case 'U':
				if(playerName == "_____________")
					playerName = "U";
				else
					playerName += "u";
				break;
			case 'v': case 'V':
				if(playerName == "_____________")
					playerName = "V";
				else
					playerName += "v";
				break;
			case 'w': case 'W':
				if(playerName == "_____________")
					playerName = "W";
				else
					playerName += "w";
				break;
			case 'x': case 'X':
				if(playerName == "_____________")
					playerName = "X";
				else
					playerName += "x";
				break;
			case 'y': case 'Y':
				if(playerName == "_____________")
					playerName = "Y";
				else
					playerName += "y";
				break;
			case 'z': case 'Z':
				if(playerName == "_____________")
					playerName = "Z";
				else
					playerName += "z";
			break;			
			default:
				break;
		}
	}
}

/*
* No exit requirements.
*/
void CharacterState::exit()
{	
}