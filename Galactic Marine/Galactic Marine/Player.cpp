//(c) Michael O'Neil 2015

#include "Player.h"
#include <iostream>

/*
* Constructor
*/
Player::Player()
{	
}

/*
* Moves Player up in map state.
* @param - float - value to move by.
*/
void Player::moveYPosUp(float move)
{
	yPos += move;
	if(yPos > 0.85)
		yPos = 0.85;
	
}

/*
* Moves Player down in map state.
* @param - float - value to move by.
*/
void Player::moveYPosDown(float move)
{
	yPos -= move;	
	if(yPos < -0.99)
		yPos = -0.99;
}

/*
* Moves Player left in map state.
* @param - float - value to move by.
*/
void Player::moveXPosDown(float move)
{
	xPos -= move;
	if(xPos < -0.99)
		xPos = -0.99;	
}

/*
* Moves Player right in map state.
* @param - float - value to move by.
*/
void Player::moveXPosUp(float move)
{
	xPos += move;	
	if(xPos > 0.85)
		xPos = 0.85;
}

/*
* Sets the players name.
* @param - std:string - string to be set.
*/
void Player::setName(std::string mName)
{
	name = mName;	 
}

/*
* Sets health for during/after combats.
* @param - int - health to be set.
*/
void Player::setHealth(int mHealth)
{
	health = mHealth;	
}

/*
* Increase player health maximum.
* @param - int - value to be set.
*/
void Player::setHealthMax(int mHealthMax)
{
	healthMax = mHealthMax;	
}

/*
* Increase player strength.
* @param - int - value to be set.
*/
void Player::setStrength(int mStrength)
{
	strength = mStrength;	
}

/*
* Increase player speed.
* @param - int - value to be set.
*/
void Player::setSpeed(int mSpeed)
{
	speed = mSpeed;	 
}

/*
* Increase player dollars.
* @param - int - value to be set.
*/
void Player::setDollars(int mDollars)
{
	dollars = mDollars;	
}

/*
* Sets the players position, used at the start of a new game.
* @param - float - x coordinate.
* @param - float - x size.
* @param - float - y coordinate.
* @param - float - y size.
*/
void Player::setPos(float mXPos, float mXSize, float mYPos, float mYSize)
{
	xPos = mXPos;
	xSize = mXSize;
	ySize = mYSize;
	yPos = mYPos;
}

/*
* Updates the players health, used by items.
* @param - int - value to be added to health.
*/
int Player::updateHealth(int Health)
{
	health += Health;
	return health;
}

/*
* Updates the players speed, used by items.
* @param - int - value to be added to speed.
*/
int Player::updateSpeed(int Speed)
{
	speed += Speed;
	return speed;
}

/*
* Updates the players strength, used by items.
* @param - int - value to be added to strength.
*/
int Player::updateStrength(int Strength)
{
	strength += Strength;
	return strength;
}

/*
* Updates the players dollars from winning a fight.
* @param - int - value to be added to dollars.
*/
int Player::updateDollars(int Dollars)
{
	dollars += Dollars;
	return dollars;
}

/*
* Draws the Player so can be called in required functions.
*/
void Player::draw()
{	
	glEnable(GL_TEXTURE_2D);
	glBindTexture( GL_TEXTURE_2D, texture);	
	glBegin(GL_POLYGON);	
	glTexCoord2d(0.0,0.0);	
	glVertex3f (xPos, yPos, 0.0); // first corner
	glTexCoord2d(1.0,0.0); 	
	glVertex3f (xPos+xSize, yPos, 0.0); // second corner
	glTexCoord2d(1.0,1.0); 
	glVertex3f (xPos+xSize, yPos+ySize, 0.0); // third corner
	glTexCoord2d(0.0,1.0); 
	glVertex3f (xPos, yPos+ySize, 0.0); // fourth corner
	glEnd();		
	glDisable(GL_TEXTURE_2D);	
}