//(c) Michael O'Neil 2015

#ifndef MAPSTATE_H
#define MAPSTATE_H

/*
* Map State - Find enemies to fight and items to boost players attributes.
*/
class MapState : public GameState {
private:
	bool level1;
	bool level2;
	bool level3;
	bool level4;
	bool level5;
	void createLevel2(Game &context);
	void createLevel3(Game &context);
	void createLevel4(Game &context);
	void createLevel5(Game &context);	
public:			
	void draw(SDL_Window * window, Game &context);	
	void init(Game &context);	
	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);			
	void enter(Game &context);
	void exit();	
};

#endif