//(c) Michael O'Neil 2015

#include "game.h"
#include "tableState.h"
#include <sstream>

/*
* Loads current high score table.
* @param - Game - the context of the game.
*/
void TableState::init(Game &context)
{
	screen = new Label();	
	std::string str;
	int score;
	std::ifstream in_file ("Highscores.txt");
	
	if(in_file.is_open())	
	{
		for(int i = 0; i < MAX; ++i)
		{	
			in_file >> str;
			names[i] = str;
			in_file >> score;			
			highScores[i] = score;
		}
	}
	else
		std::cout << "File could not open." << std::endl;
	in_file.close();		
}

/* 
* @param - Game - the context of the game.
*/
void TableState::enter(Game &context)
{	
	if(context.getUpdateScore() == true)
	{
		int i = 0;
		bool updated = false;
		while(i < MAX && updated == false)
		{
			if(context.getPlayer()->getDollars() > highScores[i])
			{
				pos = i;
				for(int j = MAX - 1; j > pos; j--)
				{
					names[j] = names[j - 1];
					highScores[j] = highScores[j - 1];
				}
				names[pos] = context.getPlayer()->getName();
				highScores[pos] = context.getPlayer()->getDollars();
				updated = true;
			}
			else
				i++;
		}
		context.deleteAllItems();
		context.deleteAllNPC();
		context.deletePlayer();
		context.setUpdateScore(false);
	}
}

/*
* Highscore table, displays the current table.
* @param - SDL_window - used for drawing to screen.
* @param - Game - the context of the game.
*/
void TableState::draw(SDL_Window * window, Game &context)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	screen->textToTexture("High Score Table", 40);
	screen->draw(-0.3, 0.8);	
	yPos = 0.6;
	for(int i = 0; i < MAX; i++)
	{
		screen->textToTexture(names[i].c_str(), 25);
		screen->draw(-0.5, yPos);	
		std::stringstream strStream;
		strStream << highScores[i];	
		screen->textToTexture(strStream.str().c_str(), 25);
		screen->draw(0.4, yPos);	
		yPos -= 0.1;
	}
			
	SDL_GL_SwapWindow(window);	
}

/*
* Handles any events that can be used in this state.
* @param - SDL_Event - used for keyboard events.
* @param - Game - the context of the game.
*/
void TableState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{			
		context.setState(context.getMainMenuState());		
	}
}

/*
* Saves the table when game exits.
*/
void TableState::exit()
{	
	std::ofstream myFile ("Highscores.txt");
	if(myFile.is_open())
	{		
		for(int i = 0; i < MAX; ++i)
		{
			myFile << names[i];			
			myFile << "\n";
			myFile << highScores[i];
			myFile << "\n";
		}	
		myFile.close();
	}
	else
		std::cout << "Unable to open file" << std::endl;	
}