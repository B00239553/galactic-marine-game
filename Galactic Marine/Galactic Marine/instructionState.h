//(c) Michael O'Neil 2015

#ifndef INSTRUCTIONSTATE_H   
#define INSTRUCTIONSTATE_H

/*
* Instructions - Displays controls and information.
*/
class InstructionState : public GameState {
private:
	bool showControls;
	bool showObjectives;
	bool showMonsters;
	bool showItems;
public:			
	void draw(SDL_Window * window, Game &context);	
	void init(Game &context);	
	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);	
	void enter(Game &context);
	void exit();	
};

#endif