//(c) Michael O'Neil 2015

#include "game.h"

//Open console window for output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

/*
* Game Entry Point - Creates game context.
*/
int main(int argc, char *argv[])
{	
	Game *newGame = new Game();
	newGame->init(); 
	newGame->run();
	delete newGame;
    return 0;
}	