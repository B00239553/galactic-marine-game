//(c) Michael O'Neil 2015

#ifndef CHARACTERSTATE_H
#define CHARACTERSTATE_H

/*
* Character Selection State - Choose your character and proceed to game.
*/
class CharacterState : public GameState {
private:	
	std::string playerName;
	bool character1;
	bool character2;
	bool character3;
public:			
	void draw(SDL_Window * window, Game &context);	
	void init(Game &context);	
	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);	
	void enter(Game &context);
	void exit();	
};

#endif