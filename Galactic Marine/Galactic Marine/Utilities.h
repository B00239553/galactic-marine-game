//(c) Michael O'Neil 2015

#ifndef UTILITIES_H
#define UTILITIES_H

/*
* Namespace - Utility, Used for error output.
* @param - char - message to state error occurred.
*/
namespace Utility
{
	void exitFatalError(char *message)
	{
		std::cout << message << " " << SDL_GetError();
		SDL_Quit();
		exit(1);
	}
}

#endif