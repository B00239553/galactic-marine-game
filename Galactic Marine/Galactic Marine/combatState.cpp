//(c) Michael O'Neil 2015

#include <SDL.h>
#include <SDL_ttf.h>
#include <GL/glew.h>
#include <cstdlib>
#include <ctime>
#include <sstream>
#include "game.h"
#include "combatState.h"

/*
* Constructor - Sets player and enemy position.
*/
CombatState::CombatState()
{	
	xpos = -0.8f;                                   
	ypos = 0.05f;
	xsize = 0.2f;
	ysize = 0.2f;
	targetXPos = 0.3f;
	targetYPos = 0.05f;	
}

/*
* @param - Game - the context of the game.
*/
void CombatState::init(Game &context)
{			
	screen = new Label();		
}

/*
* Resets variables for new battle. Takes in player/monster 
* variables so can be changed in the combat.
* @param - Game - the context of the game.
*/
void CombatState::enter(Game &context)
{			
	//resets previous battle outcome if there was one
	victory = false;
	defeat = false;
	attack = false;
	//resets damage
	pDamage = 0;
	tDamage = 0;
	totalDamage = 0;
	showDamage = false;
	//sets item chances to -1 every time combatState used so new random numbers will be generated only when combat called
	//if this was not included variables would hold values from last battle
	item = -1;
	itemChance = -1;
	//sets player variables will use
	playerHealth = context.getPlayer()->getHealth();
	playerHealthMax = context.getPlayer()->getHealthMax();
	playerSpeed = context.getPlayer()->getSpeed();	
	totalPlayerSpeed = context.getPlayer()->getSpeed();	
	playerStrength = context.getPlayer()->getStrength();
	playerDollars = context.getPlayer()->getDollars();
	//sets target variables will use
	targetHealth = context.getCurrentNPC()->getHealth();
	targetSpeed = context.getCurrentNPC()->getSpeed();
	targetStrength = context.getCurrentNPC()->getStrength();
	targetDollars = context.getCurrentNPC()->getDollars();
	targetItemChance = context.getCurrentNPC()->getItemChance();
}

/*
* Draws the player/monster in set positions, will display attributes for both.
* Will update attributes in battle.
* @param - SDL_window - used for drawing to screen.
* @param - Game - the context of the game.
*/
void CombatState::draw(SDL_Window * window, Game &context)
{		
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window	
	screen->textToTexture("Combat", 40);
	screen->draw(-0.2, 0.6);

	//draws player in set position
	glEnable(GL_TEXTURE_2D);
	glBindTexture( GL_TEXTURE_2D, context.getPlayer()->getTex());	
	glBegin(GL_POLYGON);	
	glTexCoord2d(0.0,0.0);	
    glVertex3f (xpos, ypos, 0.0); // first corner
	glTexCoord2d(1.0,0.0); 	
    glVertex3f (xpos+xsize, ypos, 0.0);// second corner
	glTexCoord2d(1.0,1.0); 
    glVertex3f (xpos+xsize, ypos+ysize, 0.0);  // third corner
	glTexCoord2d(0.0,1.0); 
    glVertex3f (xpos, ypos+ysize, 0.0);  // fourth corner
    glEnd();	
	screen->textToTexture(context.getPlayer()->getName().c_str(), 25);
	screen->draw(xpos, ypos+ysize + 0.05f);
		
	//draws monster from set position, will take in the texture from the currentNPC variable
	glEnable(GL_TEXTURE_2D);
	glBindTexture( GL_TEXTURE_2D, context.getCurrentNPC()->getTex());
	glBegin(GL_POLYGON);
	glTexCoord2d(1.0,1.0);
    glVertex3f (targetXPos, targetYPos+ysize, 0.0); // first corner
	glTexCoord2d(0.0,1.0); 
    glVertex3f (targetXPos+context.getCurrentNPC()->getXSize(), targetYPos+ysize, 0.0); // second corner
	glTexCoord2d(0.0,0.0); 
    glVertex3f (targetXPos+context.getCurrentNPC()->getXSize(), targetYPos, 0.0); // third corner
	glTexCoord2d(1.0,0.0); 
    glVertex3f (targetXPos, targetYPos, 0.0); // fourth corner
    glEnd();	
	screen->textToTexture(context.getCurrentNPC()->getName().c_str(), 25); //monsters name
	screen->draw(targetXPos, targetYPos+ysize + 0.05f);
	glDisable(GL_TEXTURE_2D);
		    
	//Outputs the players statistics
	std::stringstream strDollars;
    strDollars << "Dollars:" << context.getPlayer()->getDollars();
	screen->textToTexture(strDollars.str().c_str(), 25);
	screen->draw(-0.8,-0.2);  
	context.getPlayer()->setHealth(playerHealth);
	std::stringstream strHealth;
    strHealth << "Health:" << context.getPlayer()->getHealth();
	screen->textToTexture(strHealth.str().c_str(), 25);
	screen->draw(-0.8,-0.3);
	std::stringstream strStrength;
    strStrength << "Strength:" << context.getPlayer()->getStrength();
	screen->textToTexture(strStrength.str().c_str(), 25);
	screen->draw(-0.8,-0.4);
	std::stringstream strSpeed;
    strSpeed << "Speed:" << context.getPlayer()->getSpeed();
	screen->textToTexture(strSpeed.str().c_str(), 25);
	screen->draw(-0.8,-0.5);
	
	//Outputs the monsters statistics
	std::stringstream strMDollars;
    strMDollars << "Dollars:" << context.getCurrentNPC()->getDollars();
	screen->textToTexture(strMDollars.str().c_str(), 25);
	screen->draw(0.3,-0.2);  
	context.getCurrentNPC()->setHealth(targetHealth);
	std::stringstream strMHealth;
    strMHealth << "Health:" << context.getCurrentNPC()->getHealth();
	screen->textToTexture(strMHealth.str().c_str(), 25);
	screen->draw(0.3,-0.3);
	std::stringstream strMStrength;
    strMStrength << "Strength:" << context.getCurrentNPC()->getStrength();
	screen->textToTexture(strMStrength.str().c_str(), 25);
	screen->draw(0.3,-0.4);
	std::stringstream strMSpeed;
    strMSpeed << "Speed:" << context.getCurrentNPC()->getSpeed();
	screen->textToTexture(strMSpeed.str().c_str(), 25);
	screen->draw(0.3,-0.5);

	if(showDamage == true)
	{
		std::stringstream strP;
		strP << context.getPlayer()->getName() << " inflicted: " << pDamage << " damage";
		screen->textToTexture(strP.str().c_str(), 25);
		screen->draw(-0.4,-0.7);  

		std::stringstream strT;
		strT << context.getCurrentNPC()->getName() << " inflicted: " << tDamage << " damage";
		screen->textToTexture(strT.str().c_str(), 25);
		screen->draw(-0.4,-0.8);  
	}

	//if the player is victorious it will display the new statistics
	//and display a relevant message.
	if (victory == true)
	{
		if(totalDamage > 0)
		{
			std::stringstream damageHealed;
			damageHealed << "You have healed half of the total damage taken: " << totalDamage;
			screen->textToTexture(damageHealed.str().c_str(), 25);
			screen->draw(-0.6,-0.9);  
		}		
		context.getPlayer()->setHealth(playerHealth);	//updates health after half damage heal and if any item picked up
		context.getPlayer()->setHealthMax(playerHealthMax); //updates healthMax after half damage heal and if any item picked up
		context.getPlayer()->updateDollars(targetDollars); //updates player dollars from monster killed
		context.getPlayer()->setStrength(playerStrength); //updates strength incase any item picked up
		context.getPlayer()->setSpeed(totalPlayerSpeed); //updates speed incase any item picked up
		targetDollars = 0; //resets this value as dollars due to defeat
		targetHealth = 0; 
		context.getCurrentNPC()->setDollars(targetDollars);
		context.getCurrentNPC()->setHealth(targetHealth);
		screen->textToTexture("You beat the ", 25);
		screen->draw(-0.3, 0.3);
		screen->textToTexture(context.getCurrentNPC()->getName().c_str(), 25);
		screen->draw(0.0, 0.3);
		if(item >= 0 && item < 60) //if an item was dropped will inform the player which one
		{
			screen->textToTexture("Item obtained: HealthPack", 25);
			screen->draw(-0.3, 0.2);
		}
		if(item >= 60 && item < 80)
		{
			screen->textToTexture("Item obtained: CombatPack", 25);
			screen->draw(-0.3, 0.2);
		}
		if(item>= 80 && item < 100)
		{
			screen->textToTexture("Item obtained: Stimulant", 25);
			screen->draw(-0.3, 0.2);
		}
		
		//outputs new stats
		screen->textToTexture(strDollars.str().c_str(), 25);
		screen->draw(-0.8,-0.2);  
		screen->textToTexture(strHealth.str().c_str(), 25);
	    screen->draw(-0.8,-0.3);	
		screen->textToTexture(strMHealth.str().c_str(), 25);
		screen->draw(0.3,-0.3);
		screen->textToTexture(strMDollars.str().c_str(), 25);
		screen->draw(0.3,-0.2);  		
	}

	//if the player is defeated it will display the new statistics
	//and display a relevant message.
	if (defeat == true)
	{
		screen->textToTexture("You Lost", 25);
		screen->draw(-0.15, 0.3);
		playerHealth = 0;
		context.getPlayer()->setHealth(playerHealth);
		context.getCurrentNPC()->setHealth(targetHealth);		
		screen->textToTexture(strHealth.str().c_str(), 25);
	    screen->draw(-0.8,-0.3);	
		screen->textToTexture(strMHealth.str().c_str(), 25);
		screen->draw(0.3,-0.3);		
	}	
	
	SDL_GL_SwapWindow(window);
}

/*
* Handles any events that can be used in this state.
* @param - SDL_Event - used for keyboard events.
* @param - Game - the context of the game.
*/
void CombatState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{	
	if(attack == true && victory == false && defeat == false)
	{
		Combat();
		//Character with lowest speed is used to determine who hits next
		if(context.getPlayer()->getSpeed() > context.getCurrentNPC()->getSpeed())
		{
			playerSpeed -= context.getCurrentNPC()->getSpeed();
			if(targetSpeed == 0)
				targetSpeed = context.getCurrentNPC()->getSpeed();
			else
				targetSpeed -= context.getCurrentNPC()->getSpeed();
		}
		else if(context.getPlayer()->getSpeed() < context.getCurrentNPC()->getSpeed())
		{
			if(playerSpeed == 0)
				playerSpeed = context.getPlayer()->getSpeed();
			else
				playerSpeed -= context.getPlayer()->getSpeed();
			targetSpeed -= context.getPlayer()->getSpeed();
		}
		if(playerSpeed < 0)
			playerSpeed = context.getPlayer()->getSpeed() + playerSpeed;
		if(targetSpeed < 0)
			targetSpeed = context.getCurrentNPC()->getSpeed() + targetSpeed;
		attack = false;
		showDamage = true;
	}

	if (sdlEvent.type == SDL_KEYDOWN)
	{		
		switch( sdlEvent.key.keysym.sym )
		{			
			case SDLK_SPACE:
				attack = true;	
				if(victory == true) //player is victorious in combat
				{
					context.deleteNPC(context.getCurrentNPC()); //calls delete function 										
					context.setState(context.getMapState()); //returns to map							 				
				}
				if(defeat == true)
					context.setState(context.getGameOverState()); //goes to gameOverState if player killed
				break;				
			default:
				break;
		}
	}		
}

/*
* Calculates combat. Combat is turn based and calculated on speed. However has
* highest speed will hit first, after initial attacks speed is updated by, deducted
* the lowest speed off both characters, this means characters may be able to attack
* someone twice before retaliation.
*/
void CombatState::Combat()
{		
	if((playerHealth >= 1) && (targetHealth >= 1)) //player and monster must be alive
	{		
		if(playerSpeed > targetSpeed) //player speed highest so strikes first
		{		
			pDamage = randomDamage(2, playerStrength);
			targetHealth -= pDamage;			
			if(targetHealth >= 1 && targetSpeed > 0) //monster strikes back if still alive
			{
				tDamage = randomDamage(2, targetStrength); //damage player sustained
				playerHealth -= tDamage;	
				totalDamage += tDamage;
			}	
			else 
				tDamage = 0;
		}
		else if(playerSpeed == targetSpeed) //player speed and monster speed match so random who hits first
		{			
			int randomHit = rand() % 2;	//random number between 0 & 1, 		
			if(randomHit == 0) // 0 is player, player strikes first
			{
				pDamage = randomDamage(2, playerStrength);
				targetHealth -= pDamage;
				if(targetHealth >= 1) //monster strikes back if still alive
				{
					tDamage = randomDamage(2, targetStrength); //damage player sustained
					playerHealth -= tDamage;	
					totalDamage += tDamage;
				}						
			}
			else // 1 is monster, monster strikes first
			{
				tDamage = randomDamage(2, targetStrength); //damage player sustained
				playerHealth -= tDamage;	
				totalDamage += tDamage;
				if(playerHealth >= 1) //player strikes back if still alive
				{
					pDamage = randomDamage(2, playerStrength);
					targetHealth -= pDamage;
				}						
			}			
		}
		else //monster speed highest so strikes first
		{
			tDamage = randomDamage(2, targetStrength); //damage player sustained
			playerHealth -= tDamage;	
			totalDamage += tDamage;
			if(playerHealth >= 1 && playerSpeed > 0) //player strikes back if still alive
			{
				pDamage = randomDamage(2, playerStrength);
				targetHealth -= pDamage;
			}		
			else
				pDamage = 0;
		}		
	}	
	
	if(targetHealth <= 0) //if monster dead
	{
		victory = true;
		playerHealth += (totalDamage / 2);
		playerSpeed = totalPlayerSpeed;
		itemChance = rand() % 100;
		if(itemChance < targetItemChance) //calculates itemChance from monster type range 0 - 99 = 100%
		{
			item = rand() % 100;
			if(item >= 0 && item < 60) // 0 - 59 = 60%
			{
				if(playerHealth == playerHealthMax)
				{					
					playerHealthMax += 1;
					playerHealth = playerHealthMax;
				}				
				else
					playerHealth = playerHealthMax;					
			}
			else if(item >= 60 && item < 80) // 60 - 79 = 20%
			{
				playerStrength += 2;				
			}
			else if(item >= 80 && item < 100) // 80 - 99 = 20%
			{
				totalPlayerSpeed += 1;				
			}
		}		
	}
	if(playerHealth <= 0) //if player dead
	{
		playerSpeed = totalPlayerSpeed;
		defeat = true;		
	}		
}

/*
* Calculate random damage based on values taken in.
* @param - int - minimum value for damage.
* @param - int - maximum value for damage.
* @return - int - returns the amount of damage inflicted.
*/
int CombatState::randomDamage(int min, int max) //calculates randomDamage, uses min and max value
{	
	int randomDamage = max - min + 1;
	randomDamage = rand() % randomDamage;
	randomDamage += min;			
	return randomDamage;
}

/*
* No exit requirements.
*/
void CombatState::exit()
{
}