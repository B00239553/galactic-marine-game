//(c) Michael O'Neil 2015

#include "game.h"
#include "mainMenuState.h"

/*
* @param - Game - the context of the game.
*/
void MainMenuState::init(Game &context)
{		
	screen = new Label();
}

/*
* @param - Game - the context of the game.
*/
void MainMenuState::enter(Game &context)
{
	gameAvailable = false;
	gameAvailNewGame = false;
}

/*
* Main menu for game, continue function is only available if a
* current game is running and player has passed game. 
* @param - SDL_window - used for drawing to screen.
* @param - Game - the context of the game.
*/
void MainMenuState::draw(SDL_Window * window, Game &context)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window	
	if(gameAvailNewGame == false)
	{
		screen->textToTexture("Galatic Marine", 40);
		screen->draw(-0.3f, 0.8f);
		screen->textToTexture("Main Menu", 40);
		screen->draw(-0.25f, 0.7f);
		screen->textToTexture("New Game (N)", 25);
		screen->draw(-0.2f, 0.1f);
		screen->textToTexture("Instructions (I)", 25);
		screen->draw(-0.2f, 0.0f);
		screen->textToTexture("Highscore Table (H)", 25);
		screen->draw(-0.2f, -0.1f);
		screen->textToTexture("Credits (R)", 25);
		screen->draw(-0.2f, -0.2f);
		screen->textToTexture("Quit (Q/Esc)", 25);
		screen->draw(-0.2f, -0.3f);
		if(context.getCount() > 0) //A game is running as monsters alive.
		{
			gameAvailable = true;
			screen->textToTexture("Continue (C)", 25);
			screen->draw(-0.2f, 0.2f);
		}
	}
	else
	{
		screen->textToTexture("Current progress will be lost", 25);
		screen->draw(-0.4f, 0.1f);
		screen->textToTexture("Are you sure (Y/N)", 25);
		screen->draw(-0.3f, 0.0f);
	}			
	SDL_GL_SwapWindow(window);	
}

/*
* Handles any events that can be used in this state.
* @param - SDL_Event - used for keyboard events.
* @param - Game - the context of the game.
*/
void MainMenuState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{		
		switch( sdlEvent.key.keysym.sym )
		{
			case SDLK_ESCAPE:					
				context.setRunning(false);
				context.deleteAllItems();
				context.deleteAllNPC();
				context.deletePlayer();
				context.deleteTex(context.getTexBoss());
				context.deleteTex(context.getTexBrute());
				context.deleteTex(context.getTexFodder());
				context.deleteTex(context.getTexRaider());
				context.deleteTex(context.getTexChar1());
				context.deleteTex(context.getTexChar2());
				context.deleteTex(context.getTexChar3());
				context.deleteTex(context.getTexHealth());
				context.deleteTex(context.getTexStrength());
				context.deleteTex(context.getTexStimulant());
				break;		
			case 'n': case 'N':
				if(gameAvailNewGame == true)
				{
					gameAvailNewGame = false;
				}
				else
				{
					if(gameAvailable == false)
						context.setState(context.getCharacterState());
					else
						gameAvailNewGame = true;
				}
				break;				
			case 'c': case 'C':
				if(context.getCount() > 0)
				{
					context.setState(context.getMapState());				
				}
				break;
			case 'q': case 'Q':				
				context.setRunning(false);
				context.deleteAllItems();
				context.deleteAllNPC();
				context.deletePlayer();
				context.deleteTex(context.getTexBoss());
				context.deleteTex(context.getTexBrute());
				context.deleteTex(context.getTexFodder());
				context.deleteTex(context.getTexRaider());
				context.deleteTex(context.getTexChar1());
				context.deleteTex(context.getTexChar2());
				context.deleteTex(context.getTexChar3());
				context.deleteTex(context.getTexHealth());
				context.deleteTex(context.getTexStrength());
				context.deleteTex(context.getTexStimulant());
				break;
			case 'r': case 'R':
				context.setState(context.getCreditsState());
				break;
			case 'i': case 'I':
				context.setState(context.getInstructionsState());
				break;
			case 'h': case 'H':
				context.setState(context.getTableState());
				break;
			case 'y': case 'Y':
				if(gameAvailNewGame == true)
				{
					context.deletePlayer();
					context.deleteAllItems();
					context.deleteAllNPC();
					context.setState(context.getCharacterState());
				}
				break;
			default:
				break;
		}
	}
}

/*
* No exit requirements.
*/
void MainMenuState::exit()
{
}