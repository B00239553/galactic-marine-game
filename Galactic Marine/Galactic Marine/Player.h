//(c) Michael O'Neil 2015

#include <SDL.h>
#include <GL/glew.h>
#include <string>

#ifndef PLAYER_H
#define PLAYER_H

/*
* Player
*/
class Player {
private:
	std::string name;
	float xPos;
	float yPos;
	float xSize;
	float ySize;
	int health;
	int healthMax;
	int strength;
	int speed;
	int dollars;	
	GLuint texture;
public:
	Player();
	std::string getName(void) {return name;}
	float getXPos(void) {return xPos;}
	float getYPos(void) {return yPos;}
	float getXSize(void) {return xSize;}
	float getYSize(void) {return ySize;}
	int getHealth(void) {return health;}
	int getHealthMax(void) {return healthMax;}
	int getStrength(void) {return strength;}
	int getSpeed(void) {return speed;}
	int getDollars(void) {return dollars;}	
	GLuint getTex() {return texture;}
	void moveYPosUp(float); 
	void moveYPosDown(float);
	void moveXPosUp(float);
	void moveXPosDown(float);
	void setHealth(int);
	void setHealthMax(int);
	void setStrength(int);
	void setSpeed(int);
	void setDollars(int);	
	void setName(std::string);
	void setPos(float, float, float, float);	
	int updateHealth(int);
	int updateSpeed(int);
	int updateStrength(int);
	int updateDollars(int);
	void draw(void);	
	void setTex(GLuint tex) {texture = tex;}
};

#endif