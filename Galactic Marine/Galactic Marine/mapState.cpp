//(c) Michael O'Neil 2015

#include <SDL.h>
#include <SDL_ttf.h>
#include <GL/glew.h>
#include <ctime>
#include <sstream>
#include "game.h"
#include "mapState.h"

/*
* @param - Game - the context of the game.
*/
void MapState::init(Game &context)
{		
	glClearColor(0.0, 0.0, 0.0, 0.0); // set background colour   
    std::srand( std::time(0) ); //sets time, this is used for randomising units every new game
	screen = new Label();	
	level1 = false;
	level2 = false;
	level3 = false;
	level4 = false;
	level5 = false;
}

/*
* @param - Game - the context of the game.
*/
void MapState::enter(Game &context)
{		
	if(context.getPlayer()->getDollars() == 0)
	{ //used if new game has been started
		level1 = true;
		level2 = false;		
		level3 = false;
		level4 = false;
		level5 = false;		
	}
}

/*
* Draws the map. Will draw all monsters and items still available to interact with.
* Calls collisions on monsters and items.
* @param - SDL_window - used for drawing to screen.
* @param - Game - the context of the game.
*/
void MapState::draw(SDL_Window * window, Game &context)
{	
	int contact;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	
	//Draw Player		
	context.getPlayer()->draw();
		
	//Draws all monsters that are still alive
	for(int index = 0 ; index < context.getCount(); index++)	
		context.getNPC(index)->draw();
				
	//Draws all items not collected
	for(int index = 0 ; index < context.getItemCount(); index++)	
		context.getItems(index)->draw();		

	//used to determine if player has came into contact with a monster or item
	bool contactMonster = false;
	bool contactItem = false;

	int monster = 0, item = 0;
	//used to determine if player has contacted any part of a monster icon and which one
	while(contactMonster == false && monster < context.getCount())
	{
		contactMonster = context.checkCollision(context.getPlayer()->getXPos(), context.getPlayer()->getXSize(), context.getPlayer()->getYPos(), context.getPlayer()->getYSize(),
			context.getNPC(monster)->getXPos(), context.getNPC(monster)->getXSize(), context.getNPC(monster)->getYPos(), context.getNPC(monster)->getYSize());
		if(contactMonster == true)
			contact = monster;
		monster++;		
	}

	//used to determine if player has contacted any part of item icon and which one
	while(contactItem == false && item < context.getItemCount())
	{
		contactItem = context.checkCollision(context.getPlayer()->getXPos(), context.getPlayer()->getXSize(), context.getPlayer()->getYPos(), context.getPlayer()->getYSize(),
			context.getItems(item)->getXPos(), context.getItems(item)->getXSize(), context.getItems(item)->getYPos(), context.getItems(item)->getYSize());
		if(contactItem == true)
			contact = item;
		item++;		
	}

	if(contactMonster)
	{
		contactMonster = false; //stops loop
		context.getNPC(contact); //gets correct monster
		context.setState(context.getCombatState()); //changes to combatState
	}
		
	if(contactItem)
	{
		contactItem = false; //stops loop
		context.getItems(contact); //gets correct item
		if(context.getItems(contact)->getName() == "HealthPack") 
		{
			if(context.getPlayer()->getHealth() < context.getPlayer()->getHealthMax()) 
			{
				context.getPlayer()->setHealth(context.getPlayer()->getHealthMax()); //sets playerHealth to Max when healthpack taken as was injured
				context.deleteItem(context.getItems(contact)); //deletes item
			}
			else
			{
				context.getPlayer()->updateHealth(context.getItems(contact)->getEffect()); //updates playerHealth & playerHealthMax as wasnt injured
				context.getPlayer()->setHealthMax(context.getPlayer()->getHealth());
				context.deleteItem(context.getItems(contact)); //deletes item
			}
		}
		else if(context.getItems(contact)->getName() == "CombatPack")
		{
			context.getPlayer()->updateStrength(context.getItems(contact)->getEffect()); //updates playerStrength
			context.deleteItem(context.getItems(contact)); //deletes item
		}
		else
		{
			context.getPlayer()->updateSpeed(context.getItems(contact)->getEffect()); //updates playerSpeed
			context.deleteItem(context.getItems(contact)); //deletes item
		}		
	}		

	if(level1 == true)
	{
		screen->textToTexture("Map - Level 1", 40);
		screen->draw(-0.3, 0.9);
	}
	else if(level2 == true)
	{
		screen->textToTexture("Map - Level 2", 40);
		screen->draw(-0.3, 0.9);
	}
	else if(level3 == true)
	{
		screen->textToTexture("Map - Level 3", 40);
		screen->draw(-0.3, 0.9);
	}
	else if(level4 == true)
	{
		screen->textToTexture("Map - Level 4", 40);
		screen->draw(-0.3, 0.9);
	}
	else if(level5 == true)
	{
		screen->textToTexture("Map - Level 5", 40);
		screen->draw(-0.3, 0.9);
	}

	screen->textToTexture(context.getPlayer()->getName().c_str(), 25);
	screen->draw(-0.9, 0.9);

	//outputs player statistics
	std::stringstream strStream;
    strStream << "Dollars:" << context.getPlayer()->getDollars();
    strStream << "    Health: " << context.getPlayer()->getHealth();
	strStream << "    Strength: " << context.getPlayer()->getStrength();
	strStream << "    Speed: " << context.getPlayer()->getSpeed();
	screen->textToTexture(strStream.str().c_str(), 25);
	screen->draw(-0.9,-0.9);    

	SDL_GL_SwapWindow(window);
}

/*
* Handles any events that can be used in this state.
* @param - SDL_Event - used for keyboard events.
* @param - Game - the context of the game.
*/
void MapState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{		
	if (sdlEvent.type == SDL_KEYDOWN)
	{		
		switch( sdlEvent.key.keysym.sym )
		{
			case SDLK_UP:
			case 'w': case 'W': 
				context.getPlayer()->moveYPosUp(0.05f);					
				break;
			case SDLK_DOWN:
			case 's': case 'S':
				context.getPlayer()->moveYPosDown(0.05f);	
				break;
			case SDLK_LEFT:
			case 'a': case 'A': 
				context.getPlayer()->moveXPosDown(0.05f);	
				break;
			case SDLK_RIGHT:
			case 'd': case 'D':
				context.getPlayer()->moveXPosUp(0.05f);	
				break;
			case SDLK_ESCAPE:
				context.setState(context.getMainMenuState());
				break;			
			default:
				break;
		}
	}

	if(context.getCount() == 0 && level1 == true)
	{
		level1 = false;
		level2 = true;
		createLevel2(context);
	}
	else if(context.getCount() == 0 && level2 == true)
	{
		level2 = false;
		level3 = true;
		createLevel3(context);
	}
	else if(context.getCount() == 0 && level3 == true)
	{
		level3 = false;
		level4 = true;
		createLevel4(context);
	}
	else if(context.getCount() == 0 && level4 == true)
	{
		level4 = false;
		level5 = true;
		createLevel5(context);
	}
	else if(context.getCount() == 0 && level5 == true)
	{
		level5 = false;
		context.setState(context.getGameOverState());		
	}
}

/*
* Creates Level2.
*/
void MapState::createLevel2(Game &context)
{
	context.deleteAllItems();
	context.getPlayer()->setPos(-0.9f, 0.15f, 0.7f, 0.15f);
	context.getPlayer()->setHealth(context.getPlayer()->getHealthMax());
	context.createNPC("Raider", "Raider1", context.getTexRaider()); //creates new NPC
	while(context.collisionAtCreation(context.getNPC(0), NULL));
	context.createNPC("Brute", "Brute1", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(1), NULL));
	context.createNPC("Brute", "Brute2", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(2), NULL));
	context.createNPC("Brute", "Brute3", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(3), NULL));
	context.createNPC("Brute", "Brute4", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(4), NULL));
	context.createNPC("Fodder", "Fodder1", context.getTexFodder());
	while(context.collisionAtCreation(context.getNPC(5), NULL));
	context.createNPC("Fodder", "Fodder2", context.getTexFodder());
	while(context.collisionAtCreation(context.getNPC(6), NULL));
	context.createNPC("Fodder", "Fodder3", context.getTexFodder());
	while(context.collisionAtCreation(context.getNPC(7), NULL));
	context.createNPC("Fodder", "Fodder4", context.getTexFodder());
	while(context.collisionAtCreation(context.getNPC(8), NULL));
	context.createItem("HealthPack", "HealthPack1"); //creates new Items
	while(context.collisionAtCreation(NULL, context.getItems(0)));
	context.createItem("HealthPack", "HealthPack2");
	while(context.collisionAtCreation(NULL, context.getItems(1)));
	context.createItem("HealthPack", "HealthPack3");
	while(context.collisionAtCreation(NULL, context.getItems(2)));
	context.createItem("CombatPack", "CombatPack1");
	while(context.collisionAtCreation(NULL, context.getItems(3)));
	context.createItem("Stimulant", "Stimulant1");
	while(context.collisionAtCreation(NULL, context.getItems(4)));
	for(int index = 0 ; index < context.getCount(); index++)	//makes monsters harder in next level
		context.getNPC(index)->updateAttributes(1, 1, 1, 2);       //by boosting their stats
}

/*
* Creates Level3.
*/
void MapState::createLevel3(Game &context)
{
	context.deleteAllItems();
	context.getPlayer()->setPos(-0.9f, 0.15f, 0.7f, 0.15f);
	context.getPlayer()->setHealth(context.getPlayer()->getHealthMax());
	context.createNPC("Raider", "Raider1", context.getTexRaider()); //creates new NPC
	while(context.collisionAtCreation(context.getNPC(0), NULL));
	context.createNPC("Raider", "Raider2", context.getTexRaider());
	while(context.collisionAtCreation(context.getNPC(1), NULL));
	context.createNPC("Brute", "Brute1", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(2), NULL));
	context.createNPC("Brute", "Brute2", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(3), NULL));
	context.createNPC("Brute", "Brute3", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(4), NULL));
	context.createNPC("Brute", "Brute4", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(5), NULL));
	context.createNPC("Fodder", "Fodder1", context.getTexFodder());
	while(context.collisionAtCreation(context.getNPC(6), NULL));
	context.createNPC("Fodder", "Fodder2", context.getTexFodder());
	while(context.collisionAtCreation(context.getNPC(7), NULL));
	context.createNPC("Fodder", "Fodder3", context.getTexFodder());
	while(context.collisionAtCreation(context.getNPC(8), NULL));
	context.createItem("HealthPack", "HealthPack1"); //creates new Items
	while(context.collisionAtCreation(NULL, context.getItems(0)));
	context.createItem("HealthPack", "HealthPack2");
	while(context.collisionAtCreation(NULL, context.getItems(1)));
	context.createItem("HealthPack", "HealthPack3");
	while(context.collisionAtCreation(NULL, context.getItems(2)));
	context.createItem("CombatPack", "CombatPack2");
	while(context.collisionAtCreation(NULL, context.getItems(3)));
	context.createItem("Stimulant", "Stimulant1");
	while(context.collisionAtCreation(NULL, context.getItems(4)));
	for(int index = 0 ; index < context.getCount(); index++)	
		context.getNPC(index)->updateAttributes(2, 2, 2, 3);
}

/*
* Creates Level4.
*/
void MapState::createLevel4(Game &context)
{
	context.deleteAllItems();
	context.getPlayer()->setPos(-0.9f, 0.15f, 0.7f, 0.15f);
	context.getPlayer()->setHealth(context.getPlayer()->getHealthMax());
	context.createNPC("Raider", "Raider1", context.getTexRaider()); //creates new NPC
	while(context.collisionAtCreation(context.getNPC(0), NULL));
	context.createNPC("Raider", "Raider2", context.getTexRaider());
	while(context.collisionAtCreation(context.getNPC(1), NULL));
	context.createNPC("Raider", "Raider3", context.getTexRaider());
	while(context.collisionAtCreation(context.getNPC(2), NULL));
	context.createNPC("Brute", "Brute1", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(3), NULL));
	context.createNPC("Brute", "Brute2", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(4), NULL));
	context.createNPC("Brute", "Brute3", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(5), NULL));
	context.createNPC("Brute", "Brute4", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(6), NULL));
	context.createNPC("Fodder", "Fodder1", context.getTexFodder());
	while(context.collisionAtCreation(context.getNPC(7), NULL));
	context.createNPC("Fodder", "Fodder2", context.getTexFodder());
	while(context.collisionAtCreation(context.getNPC(8), NULL));
	context.createItem("HealthPack", "HealthPack1"); //creates new Items
	while(context.collisionAtCreation(NULL, context.getItems(0)));
	context.createItem("HealthPack", "HealthPack2");
	while(context.collisionAtCreation(NULL, context.getItems(1)));
	context.createItem("HealthPack", "HealthPack3");
	while(context.collisionAtCreation(NULL, context.getItems(2)));
	context.createItem("CombatPack", "CombatPack2");
	while(context.collisionAtCreation(NULL, context.getItems(3)));
	context.createItem("Stimulant", "Stimulant1");
	while(context.collisionAtCreation(NULL, context.getItems(4)));
	for(int index = 0 ; index < context.getCount(); index++)	
		context.getNPC(index)->updateAttributes(1, 1, 1, 4);
}

/*
* Creates Level5.
*/
void MapState::createLevel5(Game &context)
{
	context.deleteAllItems();
	context.getPlayer()->setPos(-0.9f, 0.15f, 0.7f, 0.15f);
	context.getPlayer()->setHealth(context.getPlayer()->getHealthMax());
	context.createNPC("Raider", "Raider1", context.getTexRaider()); //creates new NPC
	while(context.collisionAtCreation(context.getNPC(0), NULL));
	context.createNPC("Raider", "Raider2", context.getTexRaider());
	while(context.collisionAtCreation(context.getNPC(1), NULL));
	context.createNPC("Raider", "Raider3", context.getTexRaider());
	while(context.collisionAtCreation(context.getNPC(2), NULL));
	context.createNPC("Raider", "Raider4", context.getTexRaider());
	while(context.collisionAtCreation(context.getNPC(3), NULL));
	context.createNPC("Brute", "Brute1", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(4), NULL));
	context.createNPC("Brute", "Brute2", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(5), NULL));
	context.createNPC("Brute", "Brute3", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(6), NULL));
	context.createNPC("Brute", "Brute4", context.getTexBrute());
	while(context.collisionAtCreation(context.getNPC(7), NULL));
	context.createNPC("Boss", "The Boss", context.getTexBoss());
	while(context.collisionAtCreation(context.getNPC(8), NULL));
	context.createItem("HealthPack", "HealthPack1"); //creates new Items
	while(context.collisionAtCreation(NULL, context.getItems(0)));
	context.createItem("HealthPack", "HealthPack2");
	while(context.collisionAtCreation(NULL, context.getItems(1)));
	context.createItem("HealthPack", "HealthPack3");
	while(context.collisionAtCreation(NULL, context.getItems(2)));
	context.createItem("HealthPack", "HealthPack4");
	while(context.collisionAtCreation(NULL, context.getItems(3)));
	context.createItem("Stimulant", "Stimulant1");
	while(context.collisionAtCreation(NULL, context.getItems(4)));
	for(int index = 0 ; index < context.getCount(); index++)	
		context.getNPC(index)->updateAttributes(1, 1, 1, 4);
}

/*
* No exit requirements.
*/
void MapState::exit()
{
}