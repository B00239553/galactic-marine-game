//(c) Michael O'Neil 2015

#ifndef TABLESTATE_H   
#define TABLESTATE_H

#include <iostream>
#include <fstream>
#include <string>

/*
* High Score Table - Will load the current table and save new table on exit.
*/
class TableState : public GameState {
private:
	static const int MAX = 10;
	std::string names[MAX];
	int highScores[MAX];	
	float yPos;
	int pos;
public:			
	void draw(SDL_Window * window, Game &context);	
	void init(Game &context);	
	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);	
	void enter(Game &context);
	void exit();		
};

#endif