//(c) Michael O'Neil 2015

#include "game.h"
#include "instructionState.h"

/*
* @param - Game - the context of the game.
*/
void InstructionState::init(Game &context)
{
	screen = new Label();		
}

/*
* Set bools to false.
* @param - Game - the context of the game.
*/
void InstructionState::enter(Game &context)
{
	showControls = false;
	showObjectives = false;
	showMonsters = false;
	showItems = false;
}

/*
* Displays instructions menu, will display information depending on
* option pressed.
* @param - SDL_window - used for drawing to screen.
* @param - Game - the context of the game.
*/
void InstructionState::draw(SDL_Window * window, Game &context)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window

	if(showControls == false && showObjectives == false && showMonsters == false && showItems == false)
	{
		screen->textToTexture("Galactic Marine", 40);
		screen->draw(-0.3, 0.8);	
		screen->textToTexture("Instructions", 40);
		screen->draw(-0.25, 0.7);	
		screen->textToTexture("Controls (C)", 25);
		screen->draw(-0.2, 0.1);	
		screen->textToTexture("Objectives (O)", 25);
		screen->draw(-0.2, 0.0);	
		screen->textToTexture("Monsters (M)", 25);
		screen->draw(-0.2, -0.1);	
		screen->textToTexture("Items (I)", 25);
		screen->draw(-0.2, -0.2);	
		screen->textToTexture("Main Menu (Esc)", 25);
		screen->draw(-0.2, -0.3);	
	}
	
	if(showControls == true)
	{
		screen->textToTexture("Controls", 40);
		screen->draw(-0.2, 0.8);	
		screen->textToTexture("Movement", 30);
		screen->draw(-0.8, 0.6);
		screen->textToTexture("The arrow keys control the movement of the player on the map screen.", 25);
		screen->draw(-0.8, 0.5);
		screen->textToTexture("Press escape to pause the game and return to main menu.", 25);
		screen->draw(-0.8, 0.4);
		screen->textToTexture("Menus", 30);
		screen->draw(-0.8, 0.2);
		screen->textToTexture("Press the key displayed beside the option. If no key is shown, press any key.", 25);
		screen->draw(-0.8, 0.1);
		screen->textToTexture("Combat", 30);
		screen->draw(-0.8, -0.1);
		screen->textToTexture("Combat is turn based. Press Spacebar to attack the enemy.", 25);
		screen->draw(-0.8, -0.2);
		screen->textToTexture("Speed determines who will attack first and how often.", 25);
		screen->draw(-0.8, -0.3);
		screen->textToTexture("Strength determines how hard you or an enemy can hit.", 25);
		screen->draw(-0.8, -0.4);
		screen->textToTexture("Back (B)", 25);
		screen->draw(-0.15, -0.8);
	}

	if(showObjectives == true)
	{
		screen->textToTexture("Objective", 40);
		screen->draw(-0.2, 0.8);	
		screen->textToTexture("Kill as many monsters as possible to increase your score. Killing all", 25);
		screen->draw(-0.8, 0.6);	
		screen->textToTexture("monsters in every level will complete the game.", 25);
		screen->draw(-0.8, 0.5);			
		screen->textToTexture("Back (B)", 25);
		screen->draw(-0.15, -0.8);
	}

	if(showMonsters == true)
	{
		screen->textToTexture("Monsters", 40);
		screen->draw(-0.2, 0.8);	

		glEnable(GL_TEXTURE_2D);
		glBindTexture( GL_TEXTURE_2D, context.getTexFodder());	
		glBegin(GL_POLYGON);	
		glTexCoord2d(1.0,1.0);	
	    glVertex3f (-0.8, 0.6, 0.0); // first corner
		glTexCoord2d(0.0,1.0); 	
		glVertex3f (-0.6, 0.6, 0.0); // second corner
		glTexCoord2d(0.0,0.0); 
	    glVertex3f (-0.6, 0.4, 0.0); // third corner
		glTexCoord2d(1.0,0.0); 
	    glVertex3f (-0.8, 0.4, 0.0); // fourth corner
		glEnd();		
		
		screen->textToTexture("The Fodder - A weak monster, easily defeated.", 25); //monsters name
		screen->draw(-0.55, 0.45);

		glEnable(GL_TEXTURE_2D);
		glBindTexture( GL_TEXTURE_2D, context.getTexBrute());	
		glBegin(GL_POLYGON);	
		glTexCoord2d(1.0,1.0);	
	    glVertex3f (-0.8, 0.25, 0.0); // first corner
		glTexCoord2d(0.0,1.0); 	
		glVertex3f (-0.6, 0.25, 0.0); // second corner
		glTexCoord2d(0.0,0.0); 
	    glVertex3f (-0.6, 0.05, 0.0); // third corner
		glTexCoord2d(1.0,0.0); 
	    glVertex3f (-0.8, 0.05, 0.0); // fourth corner
		glEnd();		
		
		screen->textToTexture("The Brute - A strong but slow monster, be wary.", 25); //monsters name
		screen->draw(-0.55, 0.10);

		glEnable(GL_TEXTURE_2D);
		glBindTexture( GL_TEXTURE_2D, context.getTexRaider());	
		glBegin(GL_POLYGON);	
		glTexCoord2d(1.0,1.0);	
	    glVertex3f (-0.8, -0.05, 0.0); // first corner
		glTexCoord2d(0.0,1.0); 	
		glVertex3f (-0.6, -0.05, 0.0); // second corner
		glTexCoord2d(0.0,0.0); 
	    glVertex3f (-0.6, -0.25, 0.0); // third corner
		glTexCoord2d(1.0,0.0); 
	    glVertex3f (-0.8, -0.25, 0.0); // fourth corner
		glEnd();	
		glDisable(GL_TEXTURE_2D);
		
		screen->textToTexture("The Raider - Strong and quick, prepare yourself before", 25); //monsters name
		screen->draw(-0.55, -0.15);
		screen->textToTexture("you face them.", 25); //monsters name
		screen->draw(-0.55, -0.25);

		screen->textToTexture("Back (B)", 25);
		screen->draw(-0.15, -0.8);
	}

	if(showItems == true)
	{
		screen->textToTexture("Items", 40);
		screen->draw(-0.2, 0.8);	

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, context.getTexHealth());
		glBegin(GL_POLYGON);
		glTexCoord2d(0.0,1.0);	
		glVertex3f (-0.8, 0.6, 0.0); // first corner
		glTexCoord2d(1.0,1.0);
		glVertex3f (-0.7, 0.6, 0.0); // second corner
		glTexCoord2d(1.0,0.0);
		glVertex3f (-0.7, 0.5, 0.0); // third corner
		glTexCoord2d(0.0,0.0);
		glVertex3f (-0.8, 0.5, 0.0); // fourth corner
		glEnd();	
		
		screen->textToTexture("Healthpack - Heals you to your maximum hit points. If you are", 25); //monsters name
		screen->draw(-0.55, 0.55);
		screen->textToTexture("already at your maximum it will increase it by 1.", 25);
		screen->draw(-0.55, 0.45);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, context.getTexStimulant());
		glBegin(GL_POLYGON);
		glTexCoord2d(0.0,1.0);	
		glVertex3f (-0.8, 0.25, 0.0); // first corner
		glTexCoord2d(1.0,1.0);
		glVertex3f (-0.7, 0.25, 0.0); // second corner
		glTexCoord2d(1.0,0.0);
		glVertex3f (-0.7, 0.15, 0.0); // third corner
		glTexCoord2d(0.0,0.0);
		glVertex3f (-0.8, 0.15, 0.0); // fourth corner
		glEnd();	
		
		screen->textToTexture("Stimulant - Increases your speed by 1.", 25); //monsters name
		screen->draw(-0.55, 0.17);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, context.getTexStrength());
		glBegin(GL_POLYGON);
		glTexCoord2d(0.0,1.0);	
		glVertex3f (-0.8, -0.05, 0.0); // first corner
		glTexCoord2d(1.0,1.0);
		glVertex3f (-0.7, -0.05, 0.0); // second corner
		glTexCoord2d(1.0,0.0);
		glVertex3f (-0.7, -0.15, 0.0); // third corner
		glTexCoord2d(0.0,0.0);
		glVertex3f (-0.8, -0.15, 0.0); // fourth corner
		glEnd();			

		screen->textToTexture("Combatpack - Increases your strength by 2.", 25); //monsters name
		screen->draw(-0.55, -0.12);
		
		screen->textToTexture("Back (B)", 25);
		screen->draw(-0.15, -0.8);
	}
			
	SDL_GL_SwapWindow(window);	
}

/*
* Handles any events that can be used in this state.
* @param - SDL_Event - used for keyboard events.
* @param - Game - the context of the game.
*/
void InstructionState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{		
		switch( sdlEvent.key.keysym.sym )
		{
			case SDLK_ESCAPE:				
				context.setState(context.getMainMenuState());
				break;		
			case 'c': case 'C':
				showControls = true;
				showObjectives = false;
				showMonsters = false;
				showItems = false;
				break;				
			case 'o': case 'O':
				showControls = false;
				showObjectives = true;
				showMonsters = false;
				showItems = false;
				break;
			case 'm': case 'M':				
				showControls = false;
				showObjectives = false;
				showMonsters = true;
				showItems = false;
				break;
			case 'i': case 'I':
				showControls = false;
				showObjectives = false;
				showMonsters = false;
				showItems = true;
				break;
			case 'b': case 'B':
				showControls = false;
				showObjectives = false;
				showMonsters = false;
				showItems = false;
				break;			
			default:
				break;
		}
	}
}

/*
* No exit requirements.
*/
void InstructionState::exit()
{
}