//(c) Michael O'Neil 2015

class Game;

/*
* Abstract Item Class, other Items will inherit from this class.
*/
class Item {
protected:	
	std::string ID;
	std::string name;
	float xPos;
	float yPos;
	float xSize;
	float ySize;
	int effect;		
	GLuint texture;
public:		
	virtual ~Item() {}
	virtual float getXPos() = 0;
	virtual float getYPos() = 0;
	virtual float getXSize() = 0;
	virtual float getYSize() = 0;
	virtual GLuint getTex() = 0;
	virtual std::string getID() = 0;
	virtual std::string getName() = 0;
	virtual int getEffect() = 0;	
	virtual void draw() = 0;
	virtual void newPos() = 0;
	virtual void setTex(GLuint tex) = 0;
};