//(c) Michael O'Neil 2015

#ifndef COMBATSTATE_H
#define COMBATSTATE_H

/*
* Combat State - Fight the enemy collided into.
*/
class CombatState : public GameState {
private:		
	//used to determine whether player wins or loses battle
	bool victory;
	bool defeat;

	//used for attacking
	bool attack;

	//used to create the player character in a set position in combat
	float xpos;
	float ypos;
	float xsize;
	float ysize;
	
	//used to create the NPC's character in a set position in combat
	float targetXPos;
	float targetYPos;	

	//used to copy the players statistics, these are used in the combat sequence
	int playerHealth;
	int playerHealthMax;
	int playerSpeed;	
    int playerStrength;
	int playerDollars;
	int totalPlayerSpeed;

	//used to copy the NPC's statistices, these are used in the combat sequence
	int targetHealth;
	int targetSpeed;	
	int targetStrength;
	int targetDollars;
	int targetItemChance;

	//used to hold amount of damage inflicted
	int pDamage;
	int tDamage;
	int totalDamage;
	bool showDamage;

	//used to create change to get item and what item
	int item;
	int itemChance;

public:			
	CombatState();
	void Combat();
	int randomDamage(int min, int max);
	void draw(SDL_Window * window, Game &context);	
	void init(Game &context);	
	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);	
	void enter(Game &context);
	void exit();	
};

#endif