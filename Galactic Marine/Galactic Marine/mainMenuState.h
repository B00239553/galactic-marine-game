//(c) Michael O'Neil 2015

#ifndef MAINMENUSTATE_H   
#define MAINMENUSTATE_H

/*
* Main Menu - Possible options are selected by keyboard input.
*/
class MainMenuState : public GameState {
private:
	bool gameAvailable;
	bool gameAvailNewGame;
public:			
	void draw(SDL_Window * window, Game &context);	
	void init(Game &context);	
	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);	
	void enter(Game &context);
	void exit();	
};

#endif