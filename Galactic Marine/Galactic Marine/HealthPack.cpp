//(c) Michael O'Neil 2015

#include<cstdlib>
#include "game.h"
#include "HealthPack.h"

/*
* Constructor
* @param - std::string - ID of item.
*/
HealthPack::HealthPack(std::string mID)
{			
	ID = mID;
	name = "HealthPack";
	xPos = (float)rand()/RAND_MAX - (float)rand()/RAND_MAX;
	yPos = (float)rand()/RAND_MAX - (float)rand()/RAND_MAX;
	if(yPos > 0.9)
		yPos = 0.9;
	if(yPos < -0.99)
		yPos = -0.99;
	if(xPos > 0.9)
		xPos = 0.9;
	if(xPos < -0.99)
		xPos = -0.99;
	xSize = 0.1f;
	ySize = 0.1f;
	effect = 1;	
}

/*
* Draws the Item so can be called in required functions.
*/
void HealthPack::draw()
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);
	glBegin(GL_POLYGON);
	glTexCoord2d(0.0,0.0);	
	glVertex3f (xPos, yPos, 0.0); // first corner
	glTexCoord2d(1.0,0.0);
	glVertex3f (xPos+xSize, yPos, 0.0); // second corner
	glTexCoord2d(1.0,1.0);
	glVertex3f (xPos+xSize, yPos+ySize, 0.0); // third corner
	glTexCoord2d(0.0,1.0);
	glVertex3f (xPos, yPos+ySize, 0.0); // fourth corner
	glEnd();	
	glDisable(GL_TEXTURE_2D);	
}

/*
* Used to set new Position as collides with another object already held.
*/
void HealthPack::newPos()
{
	xPos = (float)rand()/RAND_MAX - (float)rand()/RAND_MAX;
	yPos = (float)rand()/RAND_MAX - (float)rand()/RAND_MAX;
	if(yPos > 0.9)
		yPos = 0.9;
	if(yPos < -0.99)
		yPos = -0.99;
	if(xPos > 0.9)
		xPos = 0.9;
	if(xPos < -0.99)
		xPos = -0.99;
}