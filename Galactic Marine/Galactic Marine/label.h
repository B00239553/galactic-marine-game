//(c) Michael O'Neil 2015

#include <SDL.h>
#include <SDL_ttf.h>
#include <GL/glew.h>
#include <iostream>

#ifndef LABEL_H
#define LABEL_H

/*
* Creates, draws and deletes textures for strings.
*/
class Label
{
private:	
	GLuint texID;
	GLuint width;
	GLuint height;
	SDL_Surface *stringImage;	
	void exitFatalError(char *message);	
public:
	Label();	
	void textToTexture(const char * text, int size); 	
	void draw(GLfloat x, GLfloat y);		
};

#endif 