//(c) Michael O'Neil 2015

#ifndef RAIDER_H
#define RAIDER_H

/*
* Raider Enemy
*/
class Raider : public NPC {
public:
	~Raider() {}
	Raider(std::string mID);		
	float getXPos() {return xPos;}
	float getYPos() {return yPos;}	
	float getXSize(void) {return xSize;}
	float getYSize(void) {return ySize;}
	std::string getID(void) {return ID;}
	std::string getName(void) {return name;}
	int getHealth(void) {return health;}
	int getStrength(void) {return strength;}
	int getSpeed(void) {return speed;}
	int getDollars(void) {return dollars;}
	int getItemChance() {return itemChance;}
	GLuint getTex() {return texture;}	
	void setHealth(int);
	void setDollars(int);
	void draw(void);
	void newPos(void);
	void setTex(GLuint tex) {texture = tex;}
	void updateAttributes(int, int, int, int);
};

#endif /* raider_H */ 