//(c) Michael O'Neil 2015

#include <SDL.h>
#include <SDL_ttf.h>
#include <GL/glew.h>
#include <iostream>
#include <string>
#include "gameState.h"
#include "NPC.h"
#include "Player.h"
#include "Item.h"

#ifndef GAME_H
#define GAME_H

/*
* Game - Main entry point, the game is run by changing to the different states that can be entered.
* All game elements are created and dealt with in this class as well.
*/
class Game
{
protected:	
	SDL_Window *window;
	
	GLuint char1;
	GLuint char2;
	GLuint char3;
	GLuint fodder;
	GLuint brute;
	GLuint raider;
	GLuint boss;
	GLuint health;
	GLuint strength;
	GLuint stimulant;

	GameState * currentState;
	GameState * State_intro;
	GameState * State_mainMenu;
	GameState * State_Instructions;
	GameState * State_HighScore;
	GameState * State_credits;
	GameState * State_character;
	GameState * State_map;
	GameState * State_combat;
	GameState * State_gameOver;
		
	Player * my_Player;
	NPC * currentNPC;	
	NPC* newNPC;
	Item * currentItem;
	Item* newItem;

	int count, itemCount;	
	static const int MAX = 9;
	static const int MAXITEM = 6;
	NPC *monsterArray[MAX];
	Item *itemArray[MAXITEM];

	bool updateScore;	
	bool running;	
	TTF_Font* textFont;

public:		
	Game();		
	int getCount() {return count;}
	int getItemCount() {return itemCount;}
	void init();
	void run();	
	SDL_Window * setupRC(SDL_GLContext &context);	
	void handleSDLEvent(SDL_Event const &sdlEvent);	
	void setState(GameState* newState);	
	void createPlayer();
	void deletePlayer();
	void deleteNPC(NPC* thisNPC);
	void deleteAllNPC();
	void deleteItem(Item* thisItem);
	void deleteAllItems();
	void setRunning(bool b){running = b;}	
	GameState *getState(void) {return currentState;}
	GameState *getIntroState(void) {return State_intro;}
	GameState *getMainMenuState(void) {return State_mainMenu;}
	GameState *getInstructionsState(void) {return State_Instructions;}
	GameState *getTableState(void) {return State_HighScore;}
	GameState *getCreditsState(void) {return State_credits;}
	GameState *getCharacterState(void) {return State_character;}
	GameState *getMapState(void) {return State_map;}
	GameState *getCombatState(void) {return State_combat;}
	GameState *getGameOverState(void) {return State_gameOver;}
	Player    *getPlayer(void) {return my_Player;}	
	bool collisionAtCreation(NPC* thisNPC, Item* thisItem);
	bool checkCollision(float objAX, float objAXSize, float objAY, float objAYSize, float objBX, float objBXSize, float objBY, float objBYSize);
	NPC *getNPC(int);
	NPC *getCurrentNPC() {return currentNPC;}	
	Item *getItems(int);
	Item *getCurrentItem() {return currentItem;}
	void createNPC(std::string type, std::string ID, GLuint tex);	
	void createItem(std::string type, std::string ID);
	bool getUpdateScore() {return updateScore;}
	void setUpdateScore(bool s) {updateScore = s;}	
	GLuint loadTexture(const char * filename, int w, int h);
	GLuint getTexChar1() {return char1;}
	GLuint getTexChar2() {return char2;}
	GLuint getTexChar3() {return char3;}
	GLuint getTexFodder() {return fodder;}
	GLuint getTexBrute() {return brute;}
	GLuint getTexRaider() {return raider;}
	GLuint getTexBoss() {return boss;}
	GLuint getTexHealth() {return health;}
	GLuint getTexStrength() {return strength;}
	GLuint getTexStimulant() {return stimulant;}
	void deleteTex(GLuint tex);
};

#endif 