//(c) Michael O'Neil 2015

#ifndef BRUTE_H
#define BRUTE_H

/*
* Brute Enemy
*/
class Brute : public NPC {
public:
	~Brute() {}
	Brute(std::string mID);		
	float getXPos(void) {return xPos;}
	float getYPos(void) {return yPos;}	
	float getXSize(void) {return xSize;}
	float getYSize(void) {return ySize;}
	std::string getName(void) {return name;}
	std::string getID(void) {return ID;}
	int getHealth(void) {return health;}
	int getStrength(void) {return strength;}
	int getSpeed(void) {return speed;}
	int getDollars(void) {return dollars;}
	int getItemChance() {return itemChance;}	
	GLuint getTex() {return texture;}
	void setHealth(int);
	void setDollars(int);
	void draw(void);
	void newPos(void);
	void setTex(GLuint tex) {texture = tex;}
	void updateAttributes(int, int, int, int);
};

#endif /* brute_H */ 