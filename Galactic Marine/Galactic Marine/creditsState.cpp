//(c) Michael O'Neil 2015

#include "game.h"
#include "creditsState.h"
#include <ctime>
#include <time.h>

/*
* @param - Game - the context of the game.
*/
void CreditsState::init(Game &context)
{	
	screen = new Label();
}

/*
* Clock used as timer as credits will only display for 30 seconds.
* @param - Game - the context of the game.
*/
void CreditsState::enter(Game &context)
{
	start = clock();
}

/*
* Game Credits.
* @param - SDL_window - used for drawing to screen.
* @param - Game - the context of the game.
*/
void CreditsState::draw(SDL_Window * window, Game &context)
{			
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window	
	
	screen->textToTexture("Credits", 40);
	screen->draw(-0.13f, 0.8f);
	screen->textToTexture("Based on an idea by", 25);
	screen->draw(-0.22f, 0.6f);
	screen->textToTexture("Daniel Livingstone", 25);
	screen->draw(-0.2f, 0.5f);
	screen->textToTexture("Created By", 25);
	screen->draw(-0.13f, 0.1f);
	screen->textToTexture("Michael O'Neil", 25);
	screen->draw(-0.16f, 0.0f);	
	screen->textToTexture("Artwork By", 25);
	screen->draw(-0.15f, -0.4f);
	screen->textToTexture("www.planetminecraft.com", 25);
	screen->draw(-0.3f, -0.5f);
	screen->textToTexture("http://artscum.net/portfolio/gallery.htm", 25);
	screen->draw(-0.45f, -0.6f);
	screen->textToTexture("http://www.polycount.com/forum/showthread.php?t=98767", 25);
	screen->draw(-0.65f, -0.7f);
	
	time_t finish = 30000; // 30 seconds
	if (clock() >= (finish + start)) //executes after 30 seconds reached viewing this screen
	{			
		context.setState(context.getMainMenuState());	
	}	
	SDL_GL_SwapWindow(window);		
}

/*
* Handles any events that can be used in this state.
* @param - SDL_Event - used for keyboard events.
* @param - Game - the context of the game.
*/
void CreditsState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{		
	if (sdlEvent.type == SDL_KEYDOWN)
	{		
		context.setState(context.getMainMenuState());		
	}
}

/*
* No exit requirements.
*/
void CreditsState::exit()
{
}