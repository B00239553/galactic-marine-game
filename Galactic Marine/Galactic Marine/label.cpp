//(c) Michael O'Neil 2015

#include "Label.h" 

/*
* Constructor
*/
Label::Label()
{		
	texID = 0;
}

/*
* Used for error messages.
* @param - char - message to state error occurred.
*/
void Label::exitFatalError(char *message)
{
	std::cout << message << " " << SDL_GetError();
	SDL_Quit();
	exit(1);
}

/*
* Creates texture for taken in string, size must be specified.
* @param - char * - string to be put on texture.
* @param - int - size of texture.
*/
void Label::textToTexture(const char * text, int size)
{		
	TTF_Font* TextFont;
	TextFont = TTF_OpenFont("MavenPro-Regular.ttf", size);
	std::cout << TTF_GetError() << std::endl;
	SDL_Color colour = { 255, 100, 0 }; 
	stringImage = TTF_RenderText_Blended(TextFont,text,colour);	
	width = stringImage->w;
	height = stringImage->h;
	if (stringImage == NULL)
		exitFatalError("String surface not created.");

	GLuint colours = stringImage->format->BytesPerPixel;
	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} else {             // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;
	
	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_2D, texID); 
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);	
	TTF_CloseFont(TextFont);
	
}

/*
* Will draw the string texture.
* @param - GLfloat - x position for text.
* @param - GLfloat - y position for text.
*/
void Label::draw(GLfloat x, GLfloat y)
{			
	// Draw texture here	
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glBegin(GL_QUADS);
	  glTexCoord2d(0,1); // Texture has origin at top not bottom
      glVertex3f (x,y, 0.0); // first corner
	  glTexCoord2d(1,1);
      glVertex3f (x+0.002f*width, y, 0.0); // second corner
	  glTexCoord2d(1,0);
      glVertex3f (x+0.002f*width, y+0.002f*height, 0.0); // third corner
	  glTexCoord2d(0,0);
      glVertex3f (x, y+0.002f*height, 0.0); // fourth corner
    glEnd();

	glDisable(GL_TEXTURE_2D);		
	glDeleteTextures(1, &texID);
	SDL_FreeSurface(stringImage);
}