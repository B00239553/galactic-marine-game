//(c) Michael O'Neil 2015

#include "game.h"
#include "introState.h"
#include <ctime>

/*
* @param - Game - the context of the game.
*/
void IntroState::init(Game &context)
{			
	story = false;	
	screen = new Label();
}

/*
* @param - Game - the context of the game.
*/
void IntroState::enter(Game &context)
{		
}

/*
* Displays the game intro, will change to main menu if any key
* pressed or five seconds has passed.
* @param - SDL_window - used for drawing to screen.
* @param - Game - the context of the game.
*/
void IntroState::draw(SDL_Window * window, Game &context)
{			
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	if(story == false)	
	{		
		screen->textToTexture("Nemesis Productions", 40);
		screen->draw(-0.4, 0.6f);
	}		
	if (clock() >= 2000 && story == false) //displays after 2 seconds	
	{
		screen->textToTexture("Presents", 25);
		screen->draw(-0.2f, 0.4f);
	}
	
	if (clock() >= 4000 && story == false) //displays after 4 seconds
	{
		screen->textToTexture("Galactic Marine", 80);
		screen->draw(-0.6f, -0.2f);
	}
	if (clock() >= 10000) //changes to back story after 10 seconds
	{	
		story = true;
		screen->textToTexture("Crashed on an alien world", 40);
		screen->draw(-0.5f, 0.2f);
		screen->textToTexture("inhabited by monsters", 40);
		screen->draw(-0.4f, 0.1f);
		screen->textToTexture("you must defeat them to survive!!", 40);
		screen->draw(-0.6f, 0.0f);
	}
	if (clock() >= 30000) //changes to main menu after 30 seconds
	{			
		context.setState(context.getMainMenuState());
	}	
	SDL_GL_SwapWindow(window);	
}

/*
* Handles any events that can be used in this state.
* @param - SDL_Event - used for keyboard events.
* @param - Game - the context of the game.
*/
void IntroState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{		
	if (sdlEvent.type == SDL_KEYDOWN)
	{		
		context.setState(context.getMainMenuState());		
	}
}

/*
* No exit requirements.
*/
void IntroState::exit()
{		
}